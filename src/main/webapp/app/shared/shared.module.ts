import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RecyclingSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [RecyclingSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [RecyclingSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecyclingSharedModule {
  static forRoot() {
    return {
      ngModule: RecyclingSharedModule
    };
  }
}
