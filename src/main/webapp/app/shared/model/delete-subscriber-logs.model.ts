import { Moment } from 'moment';

export interface IDelete_subscriber_logs {
  id?: number;
  msisdn?: string;
  imsi?: string;
  platform?: string;
  deleted_date?: Moment;
  deleted_status?: string;
  request_sent?: string;
  platform_response?: string;
  session_id?: string;
}

export class Delete_subscriber_logs implements IDelete_subscriber_logs {
  constructor(
    public id?: number,
    public msisdn?: string,
    public imsi?: string,
    public platform?: string,
    public deleted_date?: Moment,
    public deleted_status?: string,
    public request_sent?: string,
    public platform_response?: string,
    public session_id?: string
  ) {}
}
