import { Moment } from 'moment';

export interface IDeleted_subscriber {
  id?: number;
  msisdn?: string;
  imsi?: string;
  platform?: string;
  processing_status?: string;
  comments?: string;
  createdBy?: string;
  trxID?: string;
  createdDate?: Moment;
}

export class Deleted_subscriber implements IDeleted_subscriber {
  constructor(
    public id?: number,
    public msisdn?: string,
    public imsi?: string,
    public platform?: string,
    public processing_status?: string,
    public comments?: string,
    public createdBy?: string,
    public trxID?: string,
    public createdDate?: Moment
  ) {}
}
