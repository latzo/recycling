export interface IExclude_number {
  id?: number;
  msisdn?: string;
  comments?: string;
}

export class Exclude_number implements IExclude_number {
  constructor(public id?: number, public msisdn?: string, public comments?: string) {}
}
