export interface IDelete_subscriber {
  id?: number;
  msisdn?: string;
  imsi?: string;
  platform?: string;
  comments?: string;
}

export class Delete_subscriber implements IDelete_subscriber {
  constructor(public id?: number, public msisdn?: string, public imsi?: string, public platform?: string, public comments?: string) {}
}
