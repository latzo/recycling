import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'delete-subscriber',
        loadChildren: './delete-subscriber/delete-subscriber.module#RecyclingDelete_subscriberModule'
      },
      {
        path: 'deleted-subscriber',
        loadChildren: './deleted-subscriber/deleted-subscriber.module#RecyclingDeleted_subscriberModule'
      },
      {
        path: 'delete-subscriber-logs',
        loadChildren: './delete-subscriber-logs/delete-subscriber-logs.module#RecyclingDelete_subscriber_logsModule'
      },
      {
        path: 'exclude-number',
        loadChildren: './exclude-number/exclude-number.module#RecyclingExclude_numberModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecyclingEntityModule {}
