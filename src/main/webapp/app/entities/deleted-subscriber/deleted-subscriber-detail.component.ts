import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDeleted_subscriber } from 'app/shared/model/deleted-subscriber.model';

@Component({
  selector: 'jhi-deleted-subscriber-detail',
  templateUrl: './deleted-subscriber-detail.component.html'
})
export class Deleted_subscriberDetailComponent implements OnInit {
  deleted_subscriber: IDeleted_subscriber;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ deleted_subscriber }) => {
      this.deleted_subscriber = deleted_subscriber;
    });
  }

  previousState() {
    window.history.back();
  }
}
