import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { RecyclingSharedModule } from 'app/shared';
import {
  Deleted_subscriberComponent,
  Deleted_subscriberDetailComponent,
  Deleted_subscriberUpdateComponent,
  Deleted_subscriberDeletePopupComponent,
  Deleted_subscriberDeleteDialogComponent,
  deleted_subscriberRoute,
  deleted_subscriberPopupRoute
} from './';

const ENTITY_STATES = [...deleted_subscriberRoute, ...deleted_subscriberPopupRoute];

@NgModule({
  imports: [RecyclingSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    Deleted_subscriberComponent,
    Deleted_subscriberDetailComponent,
    Deleted_subscriberUpdateComponent,
    Deleted_subscriberDeleteDialogComponent,
    Deleted_subscriberDeletePopupComponent
  ],
  entryComponents: [
    Deleted_subscriberComponent,
    Deleted_subscriberUpdateComponent,
    Deleted_subscriberDeleteDialogComponent,
    Deleted_subscriberDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecyclingDeleted_subscriberModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
