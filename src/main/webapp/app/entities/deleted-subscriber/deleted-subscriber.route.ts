import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Deleted_subscriber } from 'app/shared/model/deleted-subscriber.model';
import { Deleted_subscriberService } from './deleted-subscriber.service';
import { Deleted_subscriberComponent } from './deleted-subscriber.component';
import { Deleted_subscriberDetailComponent } from './deleted-subscriber-detail.component';
import { Deleted_subscriberUpdateComponent } from './deleted-subscriber-update.component';
import { Deleted_subscriberDeletePopupComponent } from './deleted-subscriber-delete-dialog.component';
import { IDeleted_subscriber } from 'app/shared/model/deleted-subscriber.model';

@Injectable({ providedIn: 'root' })
export class Deleted_subscriberResolve implements Resolve<IDeleted_subscriber> {
  constructor(private service: Deleted_subscriberService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDeleted_subscriber> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Deleted_subscriber>) => response.ok),
        map((deleted_subscriber: HttpResponse<Deleted_subscriber>) => deleted_subscriber.body)
      );
    }
    return of(new Deleted_subscriber());
  }
}

export const deleted_subscriberRoute: Routes = [
  {
    path: '',
    component: Deleted_subscriberComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'recyclingApp.deleted_subscriber.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: Deleted_subscriberDetailComponent,
    resolve: {
      deleted_subscriber: Deleted_subscriberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recyclingApp.deleted_subscriber.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: Deleted_subscriberUpdateComponent,
    resolve: {
      deleted_subscriber: Deleted_subscriberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recyclingApp.deleted_subscriber.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: Deleted_subscriberUpdateComponent,
    resolve: {
      deleted_subscriber: Deleted_subscriberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recyclingApp.deleted_subscriber.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const deleted_subscriberPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: Deleted_subscriberDeletePopupComponent,
    resolve: {
      deleted_subscriber: Deleted_subscriberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recyclingApp.deleted_subscriber.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
