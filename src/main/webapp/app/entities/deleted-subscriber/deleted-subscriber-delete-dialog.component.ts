import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDeleted_subscriber } from 'app/shared/model/deleted-subscriber.model';
import { Deleted_subscriberService } from './deleted-subscriber.service';

@Component({
  selector: 'jhi-deleted-subscriber-delete-dialog',
  templateUrl: './deleted-subscriber-delete-dialog.component.html'
})
export class Deleted_subscriberDeleteDialogComponent {
  deleted_subscriber: IDeleted_subscriber;

  constructor(
    protected deleted_subscriberService: Deleted_subscriberService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.deleted_subscriberService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'deleted_subscriberListModification',
        content: 'Deleted an deleted_subscriber'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-deleted-subscriber-delete-popup',
  template: ''
})
export class Deleted_subscriberDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ deleted_subscriber }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(Deleted_subscriberDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.deleted_subscriber = deleted_subscriber;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/deleted-subscriber', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/deleted-subscriber', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
