import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDeleted_subscriber } from 'app/shared/model/deleted-subscriber.model';

import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
import moment = require('moment');

type EntityResponseType = HttpResponse<IDeleted_subscriber>;
type EntityArrayResponseType = HttpResponse<IDeleted_subscriber[]>;

@Injectable({ providedIn: 'root' })
export class Deleted_subscriberService {
  public resourceUrl = SERVER_API_URL + 'api/deleted-subscribers';

  constructor(protected http: HttpClient) {}

  create(deleted_subscriber: IDeleted_subscriber): Observable<EntityResponseType> {
    return this.http.post<IDeleted_subscriber>(this.resourceUrl, deleted_subscriber, { observe: 'response' });
  }

  update(deleted_subscriber: IDeleted_subscriber): Observable<EntityResponseType> {
    return this.http.put<IDeleted_subscriber>(this.resourceUrl, deleted_subscriber, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDeleted_subscriber>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDeleted_subscriber[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDeleted_subscriber[]>(this.resourceUrl + '/search', { params: options, observe: 'response' });
  }
  deleted: IDeleted_subscriber[];
  export(): void {
    this.http.get<IDeleted_subscriber[]>(this.resourceUrl + '/reportAll').subscribe(res => {
      console.log('les candidats: ');
      console.log('les candidats: ');
      this.deleted = res;
      let workbook = new Workbook();
      let worksheet = workbook.addWorksheet('Deleted_Subscriber');
      const title = 'Deleted Subscribers';
      const header = ['Msisdn', 'Imsi', 'Platform	Processing', 'Status', 'Created date', 'Created By', 'Transaction ID', 'Comments'];

      // Add new row
      worksheet.addRow([]);
      worksheet.addRow([]);
      let titleRow = worksheet.addRow([title]);

      // Set font, size and style in title row.
      titleRow.font = { name: 'Comic Sans MS', family: 4, size: 16, bold: true };

      // Blank Row
      worksheet.addRow([]);

      //Add row with current date
      worksheet.addRow(moment().format('LLLL'));

      //Add Header Row
      let headerRow = worksheet.addRow(header);

      // Cell Style : Fill and Border
      headerRow.eachCell((cell, number) => {
        cell.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: 'FFFFFF00' },
          bgColor: { argb: 'FF0000FF' }
        };
        cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
      });

      //add data and conditional formatting
      res.forEach(t => {
        // eslint-disable-next-line no-console

        const elt = [t.msisdn, t.imsi, t.platform, t.processing_status, t.createdDate, t.createdBy, t.trxID, t.comments];
        const eltRow = worksheet.addRow(elt);
        eltRow.eachCell((cell, number) => {
          cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
        });
      });

      workbook.xlsx.writeBuffer().then(data => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        fs.saveAs(blob, 'Deleted_Subscribers.xlsx');
      });
    });
  }

  exportSearch(req?: any): void {
    const options = createRequestOption(req);
    this.http
      .get<IDeleted_subscriber[]>(this.resourceUrl + '/reportAll/search', { params: options, observe: 'response' })
      .subscribe(res => {
        console.log('les candidats: ');
        console.log('les candidats: ');
        let workbook = new Workbook();
        let worksheet = workbook.addWorksheet('Deleted_Subscriber');
        const title = 'Deleted Subscribers';
        const header = ['Msisdn', 'Imsi', 'Platform	Processing', 'Status', 'Created date', 'Created By', 'Transaction ID', 'Comments'];

        // Add new row
        worksheet.addRow([]);
        worksheet.addRow([]);
        let titleRow = worksheet.addRow([title]);

        // Set font, size and style in title row.
        titleRow.font = { name: 'Comic Sans MS', family: 4, size: 16, bold: true };

        // Blank Row
        worksheet.addRow([]);

        //Add row with current date
        worksheet.addRow(moment().format('LLLL'));

        //Add Header Row
        let headerRow = worksheet.addRow(header);

        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: 'FFFFFF00' },
            bgColor: { argb: 'FF0000FF' }
          };
          cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
        });

        //add data and conditional formatting
        res.body.forEach(t => {
          // eslint-disable-next-line no-console

          const elt = [t.msisdn, t.imsi, t.platform, t.processing_status, t.createdDate, t.createdBy, t.trxID, t.comments];
          const eltRow = worksheet.addRow(elt);
          eltRow.eachCell((cell, number) => {
            cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
          });
        });

        workbook.xlsx.writeBuffer().then(data => {
          const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
          fs.saveAs(blob, 'Deleted_Subscribers.xlsx');
        });
      });
  }
}
