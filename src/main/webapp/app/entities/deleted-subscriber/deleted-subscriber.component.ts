import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IDeleted_subscriber } from 'app/shared/model/deleted-subscriber.model';
import { AccountService } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { Deleted_subscriberService } from './deleted-subscriber.service';
import moment = require('moment');

@Component({
  selector: 'jhi-deleted-subscriber',
  templateUrl: './deleted-subscriber.component.html'
})
export class Deleted_subscriberComponent implements OnInit, OnDestroy {
  currentAccount: any;
  deleted_subscribers: IDeleted_subscriber[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  msisdn;
  imsi;
  platform;
  status;
  debut;
  fin;
  hidespinner = true;
  search = false;
  btnSearchClicked = false;

  constructor(
    protected deleted_subscriberService: Deleted_subscriberService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  loadAll() {
    this.deleted_subscriberService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IDeleted_subscriber[]>) => this.paginateDeleted_subscribers(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/deleted-subscriber'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'desc' : 'desc')
      }
    });
    if (this.search) {
      this.loadPageSearch();
    } else {
      this.loadAll();
    }
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      '/deleted-subscriber',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'desc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInDeleted_subscribers();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IDeleted_subscriber) {
    return item.id;
  }

  registerChangeInDeleted_subscribers() {
    this.eventSubscriber = this.eventManager.subscribe('deleted_subscriberListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateDeleted_subscribers(data: IDeleted_subscriber[], headers: HttpHeaders) {
    console.log('**************2');
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.deleted_subscribers = data;
    if (data) {
      this.hidespinner = true;
    }
    console.log(data);
  }

  protected paginateDeleted_subscribers2(data: IDeleted_subscriber[], headers: HttpHeaders) {
    console.log('**************1');
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    console.log('item');
    console.log(this.totalItems);
    this.deleted_subscribers = data;
    if (data) {
      this.hidespinner = true;
      this.search = true;
    }
    console.log(data);
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  reset() {
    this.msisdn = '';
    this.platform = undefined;
    this.imsi = undefined;
    this.status = undefined;
    this.debut = '';
    this.fin = '';
    this.search = false;
    this.loadPageSearch();
  }

  loadPageSearch(page?: number): void {
    this.btnSearchClicked = true;
    const pageToLoad: number = page || this.page;
    this.hidespinner = false;
    this.deleted_subscriberService
      .search({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
        msisdn: this.msisdn || '',
        platform: this.platform || '',
        imsi: this.imsi || '',
        status: this.status || '',
        debut: this.debut !== undefined && this.debut !== '' ? new Date(this.debut).toISOString() : '',
        fin: this.fin !== undefined && this.fin !== '' ? new Date(this.fin).toISOString() : ''
      })
      .subscribe(
        (res: HttpResponse<IDeleted_subscriber[]>) => this.paginateDeleted_subscribers2(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  generateReport(): void {
    if (this.btnSearchClicked) {
      if (
        (this.msisdn == undefined || this.msisdn == '') &&
        (this.imsi == undefined || this.imsi == '') &&
        (this.platform == undefined || this.platform == '') &&
        (this.status == undefined || this.status == '') &&
        (this.debut == undefined || this.msisdn == '') &&
        (this.fin == undefined || this.fin == '')
      ) {
        this.deleted_subscriberService.export();
      } else {
        this.deleted_subscriberService.exportSearch({
          msisdn: this.msisdn || '',
          platform: this.platform || '',
          imsi: this.imsi || '',
          status: this.status || '',
          debut: this.debut !== undefined && this.debut !== '' ? new Date(this.debut).toISOString() : '',
          fin: this.fin !== undefined && this.fin !== '' ? new Date(this.fin).toISOString() : ''
        });
      }
    } else {
      this.deleted_subscriberService.export();
    }
  }
}
