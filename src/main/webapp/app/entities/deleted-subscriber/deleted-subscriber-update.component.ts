import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { IDeleted_subscriber, Deleted_subscriber } from 'app/shared/model/deleted-subscriber.model';
import { Deleted_subscriberService } from './deleted-subscriber.service';

@Component({
  selector: 'jhi-deleted-subscriber-update',
  templateUrl: './deleted-subscriber-update.component.html'
})
export class Deleted_subscriberUpdateComponent implements OnInit {
  isSaving: boolean;

  msisdn;
  imsi;
  platform;
  status;
  debut;
  fin;

  editForm = this.fb.group({
    id: [],
    msisdn: [],
    imsi: [],
    platform: [],
    processing_status: [],
    comments: []
  });

  constructor(
    protected deleted_subscriberService: Deleted_subscriberService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.router.navigateByUrl('/deleted-subscriber');
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ deleted_subscriber }) => {
      this.updateForm(deleted_subscriber);
    });
  }

  updateForm(deleted_subscriber: IDeleted_subscriber) {
    this.editForm.patchValue({
      id: deleted_subscriber.id,
      msisdn: deleted_subscriber.msisdn,
      imsi: deleted_subscriber.imsi,
      platform: deleted_subscriber.platform,
      processing_status: deleted_subscriber.processing_status,
      comments: deleted_subscriber.comments
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const deleted_subscriber = this.createFromForm();
    if (deleted_subscriber.id !== undefined) {
      this.subscribeToSaveResponse(this.deleted_subscriberService.update(deleted_subscriber));
    } else {
      this.subscribeToSaveResponse(this.deleted_subscriberService.create(deleted_subscriber));
    }
  }

  private createFromForm(): IDeleted_subscriber {
    return {
      ...new Deleted_subscriber(),
      id: this.editForm.get(['id']).value,
      msisdn: this.editForm.get(['msisdn']).value,
      imsi: this.editForm.get(['imsi']).value,
      platform: this.editForm.get(['platform']).value,
      processing_status: this.editForm.get(['processing_status']).value,
      comments: this.editForm.get(['comments']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDeleted_subscriber>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
