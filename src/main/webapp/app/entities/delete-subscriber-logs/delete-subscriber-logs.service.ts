import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDelete_subscriber_logs } from 'app/shared/model/delete-subscriber-logs.model';

type EntityResponseType = HttpResponse<IDelete_subscriber_logs>;
type EntityArrayResponseType = HttpResponse<IDelete_subscriber_logs[]>;

@Injectable({ providedIn: 'root' })
export class Delete_subscriber_logsService {
  public resourceUrl = SERVER_API_URL + 'api/delete-subscriber-logs';

  constructor(protected http: HttpClient) {}

  create(delete_subscriber_logs: IDelete_subscriber_logs): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(delete_subscriber_logs);
    return this.http
      .post<IDelete_subscriber_logs>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(delete_subscriber_logs: IDelete_subscriber_logs): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(delete_subscriber_logs);
    return this.http
      .put<IDelete_subscriber_logs>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IDelete_subscriber_logs>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDelete_subscriber_logs[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(delete_subscriber_logs: IDelete_subscriber_logs): IDelete_subscriber_logs {
    const copy: IDelete_subscriber_logs = Object.assign({}, delete_subscriber_logs, {
      deleted_date:
        delete_subscriber_logs.deleted_date != null && delete_subscriber_logs.deleted_date.isValid()
          ? delete_subscriber_logs.deleted_date.toJSON()
          : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.deleted_date = res.body.deleted_date != null ? moment(res.body.deleted_date) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((delete_subscriber_logs: IDelete_subscriber_logs) => {
        delete_subscriber_logs.deleted_date =
          delete_subscriber_logs.deleted_date != null ? moment(delete_subscriber_logs.deleted_date) : null;
      });
    }
    return res;
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDelete_subscriber_logs[]>(this.resourceUrl + '/search', { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }
}
