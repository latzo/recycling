import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDelete_subscriber_logs } from 'app/shared/model/delete-subscriber-logs.model';

let format = require('xml-formatter');

@Component({
  selector: 'jhi-delete-subscriber-logs-detail',
  templateUrl: './delete-subscriber-logs-detail.component.html'
})
export class Delete_subscriber_logsDetailComponent implements OnInit {
  delete_subscriber_logs: IDelete_subscriber_logs;
  doc: any;
  formatedXml: any;
  requestSent: any;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ delete_subscriber_logs }) => {
      this.delete_subscriber_logs = delete_subscriber_logs;
      this.doc = this.delete_subscriber_logs.platform_response;
      this.formatedXml = format(this.doc);
      this.requestSent = format(this.delete_subscriber_logs.request_sent);
    });
  }

  previousState() {
    window.history.back();
  }
}
