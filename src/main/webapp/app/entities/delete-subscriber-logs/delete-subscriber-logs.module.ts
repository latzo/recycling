import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { RecyclingSharedModule } from 'app/shared';
import {
  Delete_subscriber_logsComponent,
  Delete_subscriber_logsDetailComponent,
  Delete_subscriber_logsUpdateComponent,
  Delete_subscriber_logsDeletePopupComponent,
  Delete_subscriber_logsDeleteDialogComponent,
  delete_subscriber_logsRoute,
  delete_subscriber_logsPopupRoute
} from './';

const ENTITY_STATES = [...delete_subscriber_logsRoute, ...delete_subscriber_logsPopupRoute];

@NgModule({
  imports: [RecyclingSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    Delete_subscriber_logsComponent,
    Delete_subscriber_logsDetailComponent,
    Delete_subscriber_logsUpdateComponent,
    Delete_subscriber_logsDeleteDialogComponent,
    Delete_subscriber_logsDeletePopupComponent
  ],
  entryComponents: [
    Delete_subscriber_logsComponent,
    Delete_subscriber_logsUpdateComponent,
    Delete_subscriber_logsDeleteDialogComponent,
    Delete_subscriber_logsDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecyclingDelete_subscriber_logsModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
