import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IDelete_subscriber_logs, Delete_subscriber_logs } from 'app/shared/model/delete-subscriber-logs.model';
import { Delete_subscriber_logsService } from './delete-subscriber-logs.service';

@Component({
  selector: 'jhi-delete-subscriber-logs-update',
  templateUrl: './delete-subscriber-logs-update.component.html'
})
export class Delete_subscriber_logsUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    msisdn: [],
    imsi: [],
    platform: [],
    deleted_date: [],
    deleted_status: [],
    request_sent: [],
    platform_response: [],
    session_id: []
  });

  constructor(
    protected delete_subscriber_logsService: Delete_subscriber_logsService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.router.navigateByUrl('/delete-subscriber-logs');
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ delete_subscriber_logs }) => {
      this.updateForm(delete_subscriber_logs);
    });
  }

  updateForm(delete_subscriber_logs: IDelete_subscriber_logs) {
    this.editForm.patchValue({
      id: delete_subscriber_logs.id,
      msisdn: delete_subscriber_logs.msisdn,
      imsi: delete_subscriber_logs.imsi,
      platform: delete_subscriber_logs.platform,
      deleted_date: delete_subscriber_logs.deleted_date != null ? delete_subscriber_logs.deleted_date.format(DATE_TIME_FORMAT) : null,
      deleted_status: delete_subscriber_logs.deleted_status,
      request_sent: delete_subscriber_logs.request_sent,
      platform_response: delete_subscriber_logs.platform_response,
      session_id: delete_subscriber_logs.session_id
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const delete_subscriber_logs = this.createFromForm();
    if (delete_subscriber_logs.id !== undefined) {
      this.subscribeToSaveResponse(this.delete_subscriber_logsService.update(delete_subscriber_logs));
    } else {
      this.subscribeToSaveResponse(this.delete_subscriber_logsService.create(delete_subscriber_logs));
    }
  }

  private createFromForm(): IDelete_subscriber_logs {
    return {
      ...new Delete_subscriber_logs(),
      id: this.editForm.get(['id']).value,
      msisdn: this.editForm.get(['msisdn']).value,
      imsi: this.editForm.get(['imsi']).value,
      platform: this.editForm.get(['platform']).value,
      deleted_date:
        this.editForm.get(['deleted_date']).value != null ? moment(this.editForm.get(['deleted_date']).value, DATE_TIME_FORMAT) : undefined,
      deleted_status: this.editForm.get(['deleted_status']).value,
      request_sent: this.editForm.get(['request_sent']).value,
      platform_response: this.editForm.get(['platform_response']).value,
      session_id: this.editForm.get(['session_id']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDelete_subscriber_logs>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
