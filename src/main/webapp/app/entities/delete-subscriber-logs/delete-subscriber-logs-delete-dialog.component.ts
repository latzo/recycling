import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDelete_subscriber_logs } from 'app/shared/model/delete-subscriber-logs.model';
import { Delete_subscriber_logsService } from './delete-subscriber-logs.service';

@Component({
  selector: 'jhi-delete-subscriber-logs-delete-dialog',
  templateUrl: './delete-subscriber-logs-delete-dialog.component.html'
})
export class Delete_subscriber_logsDeleteDialogComponent {
  delete_subscriber_logs: IDelete_subscriber_logs;

  constructor(
    protected delete_subscriber_logsService: Delete_subscriber_logsService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.delete_subscriber_logsService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'delete_subscriber_logsListModification',
        content: 'Deleted an delete_subscriber_logs'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-delete-subscriber-logs-delete-popup',
  template: ''
})
export class Delete_subscriber_logsDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ delete_subscriber_logs }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(Delete_subscriber_logsDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.delete_subscriber_logs = delete_subscriber_logs;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/delete-subscriber-logs', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/delete-subscriber-logs', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
