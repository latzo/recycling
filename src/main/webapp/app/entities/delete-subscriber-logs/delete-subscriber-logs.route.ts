import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Delete_subscriber_logs } from 'app/shared/model/delete-subscriber-logs.model';
import { Delete_subscriber_logsService } from './delete-subscriber-logs.service';
import { Delete_subscriber_logsComponent } from './delete-subscriber-logs.component';
import { Delete_subscriber_logsDetailComponent } from './delete-subscriber-logs-detail.component';
import { Delete_subscriber_logsUpdateComponent } from './delete-subscriber-logs-update.component';
import { Delete_subscriber_logsDeletePopupComponent } from './delete-subscriber-logs-delete-dialog.component';
import { IDelete_subscriber_logs } from 'app/shared/model/delete-subscriber-logs.model';

@Injectable({ providedIn: 'root' })
export class Delete_subscriber_logsResolve implements Resolve<IDelete_subscriber_logs> {
  constructor(private service: Delete_subscriber_logsService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDelete_subscriber_logs> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Delete_subscriber_logs>) => response.ok),
        map((delete_subscriber_logs: HttpResponse<Delete_subscriber_logs>) => delete_subscriber_logs.body)
      );
    }
    return of(new Delete_subscriber_logs());
  }
}

export const delete_subscriber_logsRoute: Routes = [
  {
    path: '',
    component: Delete_subscriber_logsComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'recyclingApp.delete_subscriber_logs.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: Delete_subscriber_logsDetailComponent,
    resolve: {
      delete_subscriber_logs: Delete_subscriber_logsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recyclingApp.delete_subscriber_logs.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: Delete_subscriber_logsUpdateComponent,
    resolve: {
      delete_subscriber_logs: Delete_subscriber_logsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recyclingApp.delete_subscriber_logs.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: Delete_subscriber_logsUpdateComponent,
    resolve: {
      delete_subscriber_logs: Delete_subscriber_logsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recyclingApp.delete_subscriber_logs.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const delete_subscriber_logsPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: Delete_subscriber_logsDeletePopupComponent,
    resolve: {
      delete_subscriber_logs: Delete_subscriber_logsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recyclingApp.delete_subscriber_logs.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
