import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IDelete_subscriber_logs } from 'app/shared/model/delete-subscriber-logs.model';
import { AccountService } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { Delete_subscriber_logsService } from './delete-subscriber-logs.service';

@Component({
  selector: 'jhi-delete-subscriber-logs',
  templateUrl: './delete-subscriber-logs.component.html'
})
export class Delete_subscriber_logsComponent implements OnInit, OnDestroy {
  currentAccount: any;
  delete_subscriber_logs: IDelete_subscriber_logs[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  keyword: any;
  msisdn;
  imsi;
  platform;
  status;
  debut;
  fin;
  hidespinner = true;
  search = false;

  constructor(
    protected delete_subscriber_logsService: Delete_subscriber_logsService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  loadAll() {
    this.delete_subscriber_logsService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
        keyword: this.keyword
      })
      .subscribe(
        (res: HttpResponse<IDelete_subscriber_logs[]>) => this.paginateDelete_subscriber_logs(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/delete-subscriber-logs'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'desc' : 'desc')
      }
    });
    if (this.search) {
      this.loadPageSearch();
    } else {
      this.loadAll();
    }
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      '/delete-subscriber-logs',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'desc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInDelete_subscriber_logs();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IDelete_subscriber_logs) {
    return item.id;
  }

  registerChangeInDelete_subscriber_logs() {
    this.eventSubscriber = this.eventManager.subscribe('delete_subscriber_logsListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateDelete_subscriber_logs(data: IDelete_subscriber_logs[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.delete_subscriber_logs = data;
  }
  protected paginateDelete_subscriber_logs2(data: IDelete_subscriber_logs[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.delete_subscriber_logs = data;
    if (data) {
      this.search = true;
      this.hidespinner = true;
    }
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  reset() {
    this.msisdn = '';
    this.platform = undefined;
    this.imsi = undefined;
    this.status = undefined;
    this.debut = '';
    this.fin = '';
    this.loadAll();
  }

  loadPageSearch(page?: number): void {
    const pageToLoad: number = page || this.page;
    this.hidespinner = false;
    this.delete_subscriber_logsService
      .search({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
        msisdn: this.msisdn || '',
        platform: this.platform || '',
        imsi: this.imsi || '',
        status: this.status || '',
        debut: this.debut !== undefined && this.debut !== '' ? new Date(this.debut).toISOString() : '',
        fin: this.fin !== undefined && this.fin !== '' ? new Date(this.fin).toISOString() : ''
      })
      .subscribe(
        (res: HttpResponse<IDelete_subscriber_logs[]>) => this.paginateDelete_subscriber_logs2(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }
}
