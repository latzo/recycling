import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDelete_subscriber } from 'app/shared/model/delete-subscriber.model';

type EntityResponseType = HttpResponse<IDelete_subscriber>;
type EntityArrayResponseType = HttpResponse<IDelete_subscriber[]>;

@Injectable({ providedIn: 'root' })
export class Delete_subscriberService {
  public resourceUrl = SERVER_API_URL + 'api/delete-subscribers';

  constructor(protected http: HttpClient) {}

  create(delete_subscriber: IDelete_subscriber): Observable<EntityResponseType> {
    return this.http.post<IDelete_subscriber>(this.resourceUrl, delete_subscriber, { observe: 'response' });
  }

  update(delete_subscriber: IDelete_subscriber): Observable<EntityResponseType> {
    return this.http.put<IDelete_subscriber>(this.resourceUrl, delete_subscriber, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDelete_subscriber>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDelete_subscriber[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  upload(formData: FormData): Observable<any> {
    return this.http.post<any>(`${this.resourceUrl}/upload`, formData);
  }
}
