import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDelete_subscriber } from 'app/shared/model/delete-subscriber.model';
import { Delete_subscriberService } from './delete-subscriber.service';

@Component({
  selector: 'jhi-delete-subscriber-delete-dialog',
  templateUrl: './delete-subscriber-delete-dialog.component.html'
})
export class Delete_subscriberDeleteDialogComponent {
  delete_subscriber: IDelete_subscriber;

  constructor(
    protected delete_subscriberService: Delete_subscriberService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.delete_subscriberService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'delete_subscriberListModification',
        content: 'Deleted an delete_subscriber'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-delete-subscriber-delete-popup',
  template: ''
})
export class Delete_subscriberDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ delete_subscriber }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(Delete_subscriberDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.delete_subscriber = delete_subscriber;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/delete-subscriber', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/delete-subscriber', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
