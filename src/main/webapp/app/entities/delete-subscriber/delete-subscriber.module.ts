import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { RecyclingSharedModule } from 'app/shared';
import {
  Delete_subscriberComponent,
  Delete_subscriberDetailComponent,
  Delete_subscriberUpdateComponent,
  Delete_subscriberDeletePopupComponent,
  Delete_subscriberDeleteDialogComponent,
  delete_subscriberRoute,
  delete_subscriberPopupRoute
} from './';

const ENTITY_STATES = [...delete_subscriberRoute, ...delete_subscriberPopupRoute];

@NgModule({
  imports: [RecyclingSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    Delete_subscriberComponent,
    Delete_subscriberDetailComponent,
    Delete_subscriberUpdateComponent,
    Delete_subscriberDeleteDialogComponent,
    Delete_subscriberDeletePopupComponent
  ],
  entryComponents: [
    Delete_subscriberComponent,
    Delete_subscriberUpdateComponent,
    Delete_subscriberDeleteDialogComponent,
    Delete_subscriberDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecyclingDelete_subscriberModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
