import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDelete_subscriber } from 'app/shared/model/delete-subscriber.model';

@Component({
  selector: 'jhi-delete-subscriber-detail',
  templateUrl: './delete-subscriber-detail.component.html'
})
export class Delete_subscriberDetailComponent implements OnInit {
  delete_subscriber: IDelete_subscriber;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ delete_subscriber }) => {
      this.delete_subscriber = delete_subscriber;
    });
  }

  previousState() {
    window.history.back();
  }
}
