import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { IDelete_subscriber, Delete_subscriber } from 'app/shared/model/delete-subscriber.model';
import { Delete_subscriberService } from './delete-subscriber.service';

@Component({
  selector: 'jhi-delete-subscriber-update',
  templateUrl: './delete-subscriber-update.component.html'
})
export class Delete_subscriberUpdateComponent implements OnInit {
  isSaving: boolean;
  alert = '';
  isNotValidFormat = true;
  platforms = [];
  allPlatforms = '';
  allPlatforms2 = '';
  @ViewChild('form', { static: false }) form: any;
  disable = true;
  loading = false;
  file!: any;
  checkAllPlat = false;
  All = 'HLR_AUC_SWITCH_HSS';
  AllChecked = false;
  hssChecked = false;
  aucChecked = false;
  disableAll = false;
  hlrChecked;

  editForm = this.fb.group({
    id: [],
    msisdn: [],
    imsi: [],
    platform: [],
    comments: []
  });

  constructor(
    protected delete_subscriberService: Delete_subscriberService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected router: Router
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ delete_subscriber }) => {
      this.updateForm(delete_subscriber);
    });
  }

  updateForm(delete_subscriber: IDelete_subscriber) {
    this.editForm.patchValue({
      id: delete_subscriber.id,
      msisdn:
        delete_subscriber.msisdn && delete_subscriber.msisdn.startsWith('221')
          ? delete_subscriber.msisdn.replace('221', '')
          : delete_subscriber.msisdn,
      imsi: delete_subscriber.imsi,
      platform: delete_subscriber.platform,
      comments: delete_subscriber.comments
    });
  }

  previousState() {
    this.router.navigateByUrl('/delete-subscriber');
  }

  save() {
    this.isSaving = true;
    const delete_subscriber = this.createFromForm();

    if (delete_subscriber.id === undefined) {
      this.subscribeToSaveResponse(this.delete_subscriberService.create(delete_subscriber));
    } else {
      this.subscribeToSaveResponse(this.delete_subscriberService.update(delete_subscriber));
    }
  }

  private createFromForm(): IDelete_subscriber {
    return {
      ...new Delete_subscriber(),
      id: this.editForm.get(['id']).value,
      msisdn: this.editForm.get(['msisdn']).value,
      imsi: this.editForm.get(['imsi']).value,
      platform: this.allPlatforms,
      comments: this.editForm.get(['comments']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDelete_subscriber>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  uploadFile(): void {
    this.alert = '';
    this.disable = true;
    this.loading = true;
    const formData = new FormData();
    formData.append('file', this.file);
    this.delete_subscriberService.upload(formData).subscribe(res => {
      // eslint-disable-next-line no-console
      if (res !== null) {
        this.loading = false;
        this.form.nativeElement.reset();
        setInterval(() => {
          this.alert = '';
        }, 6000);
        this.alert = res.key;
      } else {
        this.router.navigateByUrl('/delete-subscriber');
      }
    });
  }
  onFileChange(event: any): void {
    this.disable = false;
    this.isNotValidFormat = true;
    this.file = event.target.files.length > 0 ? event.target.files[0] : null;

    if (!this.file.name.endsWith('.csv')) {
      this.isNotValidFormat = false;
      this.disable = true;
    }
  }

  checkPlatform(event): void {
    this.platforms.push(event.target.value);
    if (this.allPlatforms.length > 0) {
      this.allPlatforms = !this.allPlatforms.includes(event.target.value)
        ? this.allPlatforms + '_' + event.target.value
        : this.allPlatforms
            .replace('_' + event.target.value, '')
            .replace(event.target.value, '')
            .replace(event.target.value + '_', '');
      if (this.allPlatforms.startsWith('_')) {
        this.allPlatforms = this.allPlatforms.substring(1, this.allPlatforms.length);
      }
    } else {
      this.allPlatforms = event.target.value;
    }
    this.checkAllPlat = false;
    //  this.AllChecked = this.All === this.allPlatforms ? true : false
    this.AllChecked = false;
    this.allPlatforms2 = this.allPlatforms;
  }

  checkAll(event): void {
    console.log('check');
    console.log(event.target.checked);
    this.checkAllPlat = event.target.checked ? true : false;
    this.AllChecked = event.target.checked ? true : false;
    if (this.checkAllPlat) {
      this.disableAll = true;
      this.allPlatforms = this.All;
    } else {
      this.disableAll = false;
      this.allPlatforms = this.allPlatforms2;
    }
  }
}
