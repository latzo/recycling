import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Delete_subscriber } from 'app/shared/model/delete-subscriber.model';
import { Delete_subscriberService } from './delete-subscriber.service';
import { Delete_subscriberComponent } from './delete-subscriber.component';
import { Delete_subscriberDetailComponent } from './delete-subscriber-detail.component';
import { Delete_subscriberUpdateComponent } from './delete-subscriber-update.component';
import { Delete_subscriberDeletePopupComponent } from './delete-subscriber-delete-dialog.component';
import { IDelete_subscriber } from 'app/shared/model/delete-subscriber.model';

@Injectable({ providedIn: 'root' })
export class Delete_subscriberResolve implements Resolve<IDelete_subscriber> {
  constructor(private service: Delete_subscriberService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDelete_subscriber> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Delete_subscriber>) => response.ok),
        map((delete_subscriber: HttpResponse<Delete_subscriber>) => delete_subscriber.body)
      );
    }
    return of(new Delete_subscriber());
  }
}

export const delete_subscriberRoute: Routes = [
  {
    path: '',
    component: Delete_subscriberComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'recyclingApp.delete_subscriber.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: Delete_subscriberDetailComponent,
    resolve: {
      delete_subscriber: Delete_subscriberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recyclingApp.delete_subscriber.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: Delete_subscriberUpdateComponent,
    resolve: {
      delete_subscriber: Delete_subscriberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recyclingApp.delete_subscriber.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: Delete_subscriberUpdateComponent,
    resolve: {
      delete_subscriber: Delete_subscriberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recyclingApp.delete_subscriber.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const delete_subscriberPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: Delete_subscriberDeletePopupComponent,
    resolve: {
      delete_subscriber: Delete_subscriberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recyclingApp.delete_subscriber.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
