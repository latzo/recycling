import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { IExclude_number, Exclude_number } from 'app/shared/model/exclude-number.model';
import { Exclude_numberService } from './exclude-number.service';

@Component({
  selector: 'jhi-exclude-number-update',
  templateUrl: './exclude-number-update.component.html'
})
export class Exclude_numberUpdateComponent implements OnInit {
  isSaving: boolean;
  disable = true;
  loading = false;
  file!: any;
  alert = '';
  isNotValidFormat = true;
  @ViewChild('form', { static: false }) form: any;

  editForm = this.fb.group({
    id: [],
    msisdn: [],
    comments: []
  });

  constructor(
    protected exclude_numberService: Exclude_numberService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected router: Router
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ exclude_number }) => {
      this.updateForm(exclude_number);
    });
  }

  updateForm(exclude_number: IExclude_number) {
    this.editForm.patchValue({
      id: exclude_number.id,
      msisdn: exclude_number.msisdn,
      comments: exclude_number.comments
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const exclude_number = this.createFromForm();
    if (exclude_number.id !== undefined) {
      this.subscribeToSaveResponse(this.exclude_numberService.update(exclude_number));
    } else {
      this.subscribeToSaveResponse(this.exclude_numberService.create(exclude_number));
    }
  }

  private createFromForm(): IExclude_number {
    return {
      ...new Exclude_number(),
      id: this.editForm.get(['id']).value,
      msisdn: this.editForm.get(['msisdn']).value,
      comments: this.editForm.get(['comments']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExclude_number>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  uploadFile(): void {
    this.alert = '';
    this.disable = true;
    this.loading = true;
    const formData = new FormData();
    formData.append('file', this.file);
    this.exclude_numberService.upload(formData).subscribe(res => {
      // eslint-disable-next-line no-console
      if (res !== null) {
        this.loading = false;
        this.alert = res.key;
        this.form.nativeElement.reset();
      } else {
        this.router.navigateByUrl('/exclude-number');
      }
    });
  }
  onFileChange(event: any): void {
    this.disable = false;
    this.isNotValidFormat = true;
    this.file = event.target.files.length > 0 ? event.target.files[0] : null;

    if (!this.file.name.endsWith('.csv')) {
      this.isNotValidFormat = false;
      this.disable = true;
    }
  }
}
