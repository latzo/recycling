import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Exclude_number } from 'app/shared/model/exclude-number.model';
import { Exclude_numberService } from './exclude-number.service';
import { Exclude_numberComponent } from './exclude-number.component';
import { Exclude_numberDetailComponent } from './exclude-number-detail.component';
import { Exclude_numberUpdateComponent } from './exclude-number-update.component';
import { Exclude_numberDeletePopupComponent } from './exclude-number-delete-dialog.component';
import { IExclude_number } from 'app/shared/model/exclude-number.model';

@Injectable({ providedIn: 'root' })
export class Exclude_numberResolve implements Resolve<IExclude_number> {
  constructor(private service: Exclude_numberService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IExclude_number> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Exclude_number>) => response.ok),
        map((exclude_number: HttpResponse<Exclude_number>) => exclude_number.body)
      );
    }
    return of(new Exclude_number());
  }
}

export const exclude_numberRoute: Routes = [
  {
    path: '',
    component: Exclude_numberComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'recyclingApp.exclude_number.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: Exclude_numberDetailComponent,
    resolve: {
      exclude_number: Exclude_numberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recyclingApp.exclude_number.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: Exclude_numberUpdateComponent,
    resolve: {
      exclude_number: Exclude_numberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recyclingApp.exclude_number.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: Exclude_numberUpdateComponent,
    resolve: {
      exclude_number: Exclude_numberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recyclingApp.exclude_number.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const exclude_numberPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: Exclude_numberDeletePopupComponent,
    resolve: {
      exclude_number: Exclude_numberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recyclingApp.exclude_number.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
