import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IExclude_number } from 'app/shared/model/exclude-number.model';

@Component({
  selector: 'jhi-exclude-number-detail',
  templateUrl: './exclude-number-detail.component.html'
})
export class Exclude_numberDetailComponent implements OnInit {
  exclude_number: IExclude_number;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ exclude_number }) => {
      this.exclude_number = exclude_number;
    });
  }

  previousState() {
    window.history.back();
  }
}
