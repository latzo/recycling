import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { RecyclingSharedModule } from 'app/shared';
import {
  Exclude_numberComponent,
  Exclude_numberDetailComponent,
  Exclude_numberUpdateComponent,
  Exclude_numberDeletePopupComponent,
  Exclude_numberDeleteDialogComponent,
  exclude_numberRoute,
  exclude_numberPopupRoute
} from './';

const ENTITY_STATES = [...exclude_numberRoute, ...exclude_numberPopupRoute];

@NgModule({
  imports: [RecyclingSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    Exclude_numberComponent,
    Exclude_numberDetailComponent,
    Exclude_numberUpdateComponent,
    Exclude_numberDeleteDialogComponent,
    Exclude_numberDeletePopupComponent
  ],
  entryComponents: [
    Exclude_numberComponent,
    Exclude_numberUpdateComponent,
    Exclude_numberDeleteDialogComponent,
    Exclude_numberDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecyclingExclude_numberModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
