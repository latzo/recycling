import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IExclude_number } from 'app/shared/model/exclude-number.model';
import { Exclude_numberService } from './exclude-number.service';

@Component({
  selector: 'jhi-exclude-number-delete-dialog',
  templateUrl: './exclude-number-delete-dialog.component.html'
})
export class Exclude_numberDeleteDialogComponent {
  exclude_number: IExclude_number;

  constructor(
    protected exclude_numberService: Exclude_numberService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.exclude_numberService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'exclude_numberListModification',
        content: 'Deleted an exclude_number'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-exclude-number-delete-popup',
  template: ''
})
export class Exclude_numberDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ exclude_number }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(Exclude_numberDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.exclude_number = exclude_number;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/exclude-number', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/exclude-number', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
