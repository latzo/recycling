import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IExclude_number } from 'app/shared/model/exclude-number.model';

type EntityResponseType = HttpResponse<IExclude_number>;
type EntityArrayResponseType = HttpResponse<IExclude_number[]>;

@Injectable({ providedIn: 'root' })
export class Exclude_numberService {
  public resourceUrl = SERVER_API_URL + 'api/exclude-numbers';

  constructor(protected http: HttpClient) {}

  create(exclude_number: IExclude_number): Observable<EntityResponseType> {
    return this.http.post<IExclude_number>(this.resourceUrl, exclude_number, { observe: 'response' });
  }

  update(exclude_number: IExclude_number): Observable<EntityResponseType> {
    return this.http.put<IExclude_number>(this.resourceUrl, exclude_number, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IExclude_number>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IExclude_number[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
  upload(formData: FormData): Observable<any> {
    return this.http.post<any>(`${this.resourceUrl}/upload`, formData);
  }
}
