package free.sn.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A Delete_subscriber_logs.
 */
@Entity
@Table(name = "delete_subscriber_logs")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Delete_subscriber_logs implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name = "imsi")
    private String imsi;

    @Column(name = "platform")
    private String platform;

    @Column(name = "deleted_date")
    private ZonedDateTime deleted_date;

    @Column(name = "deleted_status")
    private String deleted_status;

    @Column(name = "request_sent")
    private String request_sent;

    @Column(name = "platform_response")
    private String platform_response;

    @Column(name = "session_id")
    private String session_id;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public Delete_subscriber_logs msisdn(String msisdn) {
        this.msisdn = msisdn;
        return this;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getImsi() {
        return imsi;
    }

    public Delete_subscriber_logs imsi(String imsi) {
        this.imsi = imsi;
        return this;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getPlatform() {
        return platform;
    }

    public Delete_subscriber_logs platform(String platform) {
        this.platform = platform;
        return this;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public ZonedDateTime getDeleted_date() {
        return deleted_date;
    }

    public Delete_subscriber_logs deleted_date(ZonedDateTime deleted_date) {
        this.deleted_date = deleted_date;
        return this;
    }

    public void setDeleted_date(ZonedDateTime deleted_date) {
        this.deleted_date = deleted_date;
    }

    public String getDeleted_status() {
        return deleted_status;
    }

    public Delete_subscriber_logs deleted_status(String deleted_status) {
        this.deleted_status = deleted_status;
        return this;
    }

    public void setDeleted_status(String deleted_status) {
        this.deleted_status = deleted_status;
    }

    public String getRequest_sent() {
        return request_sent;
    }

    public Delete_subscriber_logs request_sent(String request_sent) {
        this.request_sent = request_sent;
        return this;
    }

    public void setRequest_sent(String request_sent) {
        this.request_sent = request_sent;
    }

    public String getPlatform_response() {
        return platform_response;
    }

    public Delete_subscriber_logs platform_response(String platform_response) {
        this.platform_response = platform_response;
        return this;
    }

    public void setPlatform_response(String platform_response) {
        this.platform_response = platform_response;
    }

    public String getSession_id() {
        return session_id;
    }

    public Delete_subscriber_logs session_id(String session_id) {
        this.session_id = session_id;
        return this;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Delete_subscriber_logs)) {
            return false;
        }
        return id != null && id.equals(((Delete_subscriber_logs) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Delete_subscriber_logs{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", imsi='" + getImsi() + "'" +
            ", platform='" + getPlatform() + "'" +
            ", deleted_date='" + getDeleted_date() + "'" +
            ", deleted_status='" + getDeleted_status() + "'" +
            ", request_sent='" + getRequest_sent() + "'" +
            ", platform_response='" + getPlatform_response() + "'" +
            ", session_id='" + getSession_id() + "'" +
            "}";
    }
}
