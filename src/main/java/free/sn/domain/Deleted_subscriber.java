package free.sn.domain;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A Deleted_subscriber.
 */
@Entity
@Table(name = "deleted_subscriber")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Getter
@Setter
public class Deleted_subscriber extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name = "imsi")
    private String imsi;

    @Column(name = "platform")
    private String platform;

    @Column(name = "processing_status")
    private String processing_status;

    @Column(name = "comments")
    private String comments;

//    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getMsisdn() {
//        return msisdn;
//    }
//
//    public Deleted_subscriber msisdn(String msisdn) {
//        this.msisdn = msisdn;
//        return this;
//    }
//
//    public void setMsisdn(String msisdn) {
//        this.msisdn = msisdn;
//    }
//
//    public String getImsi() {
//        return imsi;
//    }
//
//    public Deleted_subscriber imsi(String imsi) {
//        this.imsi = imsi;
//        return this;
//    }
//
//    public void setImsi(String imsi) {
//        this.imsi = imsi;
//    }
//
//    public String getPlatform() {
//        return platform;
//    }
//
//    public Deleted_subscriber platform(String platform) {
//        this.platform = platform;
//        return this;
//    }
//
//    public void setPlatform(String platform) {
//        this.platform = platform;
//    }
//
//    public String getProcessing_status() {
//        return processing_status;
//    }
//
//    public Deleted_subscriber processing_status(String processing_status) {
//        this.processing_status = processing_status;
//        return this;
//    }
//
//    public void setProcessing_status(String processing_status) {
//        this.processing_status = processing_status;
//    }
//
//    public String getComments() {
//        return comments;
//    }
//
//    public Deleted_subscriber comments(String comments) {
//        this.comments = comments;
//        return this;
//    }
//
//    public void setComments(String comments) {
//        this.comments = comments;
//    }
//    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Deleted_subscriber)) {
            return false;
        }
        return id != null && id.equals(((Deleted_subscriber) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Deleted_subscriber{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", imsi='" + getImsi() + "'" +
            ", platform='" + getPlatform() + "'" +
            ", processing_status='" + getProcessing_status() + "'" +
            ", comments='" + getComments() + "'" +
            ", created_date='" + getCreatedDate() + "'" +
            "}";
    }
}
