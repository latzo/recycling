package free.sn.repository;

import free.sn.domain.Delete_subscriber_logs;
import free.sn.domain.Deleted_subscriber;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;


/**
 * Spring Data  repository for the Deleted_subscriber entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Deleted_subscriberRepository extends JpaRepository<Deleted_subscriber, Long> {

    @Query("select  d from Deleted_subscriber  d order by d.createdDate desc")
    Page<Deleted_subscriber> findAll(Pageable pageable);

    @Query("select  d from Deleted_subscriber  d order by d.createdDate desc")
    List<Deleted_subscriber> findAllReport();



    List<Deleted_subscriber> findByCreatedDateBetween(Instant debut, Instant fin);
    Page<Deleted_subscriber> findByCreatedDateBetween(Pageable pageable, Instant debut, Instant fin);
    List<Deleted_subscriber> findByCreatedDateBefore(Instant fin);
    Page<Deleted_subscriber> findByCreatedDateBefore(Pageable pageable, Instant fin);
    List<Deleted_subscriber> findByCreatedDateAfter(Instant fin);

    @Query("select d from  Deleted_subscriber d where (d.msisdn like :msisdn or :msisdn is null or :msisdn = '') and" +
        " (d.imsi like :imsi or :imsi is null or :imsi = '') and (d.platform like :platform or :platform is null or :platform = '') " +
        "and (d.processing_status like :status or :status is null or :status='') and (d.createdDate >= :debut or :debut is null or :debut='')" +
        "and (d.createdDate <= :fin or :fin is null or :fin='') order by d.createdDate desc")
    Page<Deleted_subscriber> search(Pageable pageable, @Param("msisdn") String msisdn, @Param("imsi") String imsi,
                                        @Param("platform") String platform, @Param("status") String status, @Param("debut") Instant debut, @Param("fin") Instant fin );


    @Query("select d from  Deleted_subscriber d where (d.msisdn like :msisdn or :msisdn is null or :msisdn = '') and" +
        " (d.imsi like :imsi or :imsi is null or :imsi = '') and (d.platform like :platform or :platform is null or :platform = '') " +
        "and (d.processing_status like :status or :status is null or :status='') and (d.createdDate >= :debut or :debut is null or :debut='')" +
        "and (d.createdDate <= :fin or :fin is null or :fin='') order by d.createdDate desc")
    List<Deleted_subscriber> exportSearch(@Param("msisdn") String msisdn, @Param("imsi") String imsi,
                                    @Param("platform") String platform, @Param("status") String status, @Param("debut") Instant debut, @Param("fin") Instant fin );


}
