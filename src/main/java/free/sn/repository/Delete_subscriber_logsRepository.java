package free.sn.repository;

import free.sn.domain.Delete_subscriber_logs;
import free.sn.domain.Deleted_subscriber;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;


/**
 * Spring Data  repository for the Delete_subscriber_logs entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Delete_subscriber_logsRepository extends JpaRepository<Delete_subscriber_logs, Long> {

    @Query("select d from  Delete_subscriber_logs d where d.deleted_date >= :debut and d.deleted_date <= :fin")
    List<Delete_subscriber_logs> findByDeletedDateBetween(@Param("debut") ZonedDateTime debut, @Param("fin") ZonedDateTime fin);

    @Query("select d from  Delete_subscriber_logs d where d.deleted_date <= :fin")
    List<Delete_subscriber_logs> findByDeletedBefore(@Param("fin") ZonedDateTime fin);

    @Query("select d from  Delete_subscriber_logs d where d.deleted_date >= :debut")
    List<Delete_subscriber_logs> findByDeletedDateAfter(@Param("debut") ZonedDateTime debut);

    @Query("select d from  Delete_subscriber_logs d where (d.msisdn like :msisdn or :msisdn is null or :msisdn = '') and" +
        " (d.imsi like :imsi or :imsi is null or :imsi = '') and (d.platform like :platform or :platform is null or :platform = '') " +
        "and (d.deleted_status like :status or :status is null or :status='') and (d.deleted_date >= :debut or :debut is null or :debut='') and (d.deleted_date <= :fin or :fin is null or :fin='')")
    Page<Delete_subscriber_logs> search(Pageable pageable, @Param("msisdn") String msisdn, @Param("imsi") String imsi,
                                        @Param("platform") String platform, @Param("status") String status, @Param("debut") ZonedDateTime debut, @Param("fin") ZonedDateTime fin );

}
