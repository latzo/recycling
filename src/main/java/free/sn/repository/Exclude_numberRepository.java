package free.sn.repository;

import free.sn.domain.Exclude_number;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the Exclude_number entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Exclude_numberRepository extends JpaRepository<Exclude_number, Long> {

    Optional<Exclude_number> findByMsisdn(String msisdn);
}
