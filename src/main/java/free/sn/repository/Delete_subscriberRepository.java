package free.sn.repository;

import free.sn.domain.Delete_subscriber;
import free.sn.domain.Delete_subscriber_logs;
import free.sn.domain.Deleted_subscriber;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Delete_subscriber entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Delete_subscriberRepository extends JpaRepository<Delete_subscriber, Long> {
    Page<Delete_subscriber> findAllByOrderByCreatedDateDesc(Pageable pageable);
    @Query("select d from Delete_subscriber d where d.msisdn like :keyword or d.imsi like :keyword or" +
        " d.platform like :keyword order by d.createdDate desc")
    Page<Delete_subscriber> findAllByKeyword(@Param("keyword") String keyword, Pageable pageable);
}
