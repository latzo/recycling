package free.sn.service;

import free.sn.domain.Delete_subscriber_logs;

import free.sn.domain.Deleted_subscriber;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Delete_subscriber_logs}.
 */
public interface Delete_subscriber_logsService {

    /**
     * Save a delete_subscriber_logs.
     *
     * @param delete_subscriber_logs the entity to save.
     * @return the persisted entity.
     */
    Delete_subscriber_logs save(Delete_subscriber_logs delete_subscriber_logs);

    /**
     * Get all the delete_subscriber_logs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Delete_subscriber_logs> findAll(Pageable pageable);

    /**
     * Get the "id" delete_subscriber_logs.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Delete_subscriber_logs> findOne(Long id);

    /**
     * Delete the "id" delete_subscriber_logs.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Delete the "id" delete_subscriber_logs.
     *
     * @param pageable the id of the entity.
     * @param msisdn the id of the entity.
     * @param ismi the id of the entity.
     * @param platform the id of the entity.
     * @param status the id of the entity.
     * @param debut the id of the entity.
     * @param fin the id of the entity.
     */
    List<Delete_subscriber_logs> findBySearch(Pageable pageable, String msisdn, String ismi, String platform, String status, ZonedDateTime debut, ZonedDateTime fin);

    Page<Delete_subscriber_logs> search(Pageable pageable, String msisdn, String ismi, String platform, String status, ZonedDateTime debut, ZonedDateTime fin);
}
