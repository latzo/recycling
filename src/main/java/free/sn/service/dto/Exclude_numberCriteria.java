package free.sn.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link free.sn.domain.Exclude_number} entity. This class is used
 * in {@link free.sn.web.rest.Exclude_numberResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /exclude-numbers?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class Exclude_numberCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter msisdn;

    private StringFilter comments;

    public Exclude_numberCriteria(){
    }

    public Exclude_numberCriteria(Exclude_numberCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.msisdn = other.msisdn == null ? null : other.msisdn.copy();
        this.comments = other.comments == null ? null : other.comments.copy();
    }

    @Override
    public Exclude_numberCriteria copy() {
        return new Exclude_numberCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(StringFilter msisdn) {
        this.msisdn = msisdn;
    }

    public StringFilter getComments() {
        return comments;
    }

    public void setComments(StringFilter comments) {
        this.comments = comments;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Exclude_numberCriteria that = (Exclude_numberCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(msisdn, that.msisdn) &&
            Objects.equals(comments, that.comments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        msisdn,
        comments
        );
    }

    @Override
    public String toString() {
        return "Exclude_numberCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (msisdn != null ? "msisdn=" + msisdn + ", " : "") +
                (comments != null ? "comments=" + comments + ", " : "") +
            "}";
    }

}
