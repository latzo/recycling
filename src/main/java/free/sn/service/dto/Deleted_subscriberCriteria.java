package free.sn.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link free.sn.domain.Deleted_subscriber} entity. This class is used
 * in {@link free.sn.web.rest.Deleted_subscriberResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /deleted-subscribers?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class Deleted_subscriberCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter msisdn;

    private StringFilter imsi;

    private StringFilter platform;

    private StringFilter processing_status;

    private StringFilter comments;

    public Deleted_subscriberCriteria(){
    }

    public Deleted_subscriberCriteria(Deleted_subscriberCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.msisdn = other.msisdn == null ? null : other.msisdn.copy();
        this.imsi = other.imsi == null ? null : other.imsi.copy();
        this.platform = other.platform == null ? null : other.platform.copy();
        this.processing_status = other.processing_status == null ? null : other.processing_status.copy();
        this.comments = other.comments == null ? null : other.comments.copy();
    }

    @Override
    public Deleted_subscriberCriteria copy() {
        return new Deleted_subscriberCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(StringFilter msisdn) {
        this.msisdn = msisdn;
    }

    public StringFilter getImsi() {
        return imsi;
    }

    public void setImsi(StringFilter imsi) {
        this.imsi = imsi;
    }

    public StringFilter getPlatform() {
        return platform;
    }

    public void setPlatform(StringFilter platform) {
        this.platform = platform;
    }

    public StringFilter getProcessing_status() {
        return processing_status;
    }

    public void setProcessing_status(StringFilter processing_status) {
        this.processing_status = processing_status;
    }

    public StringFilter getComments() {
        return comments;
    }

    public void setComments(StringFilter comments) {
        this.comments = comments;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Deleted_subscriberCriteria that = (Deleted_subscriberCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(msisdn, that.msisdn) &&
            Objects.equals(imsi, that.imsi) &&
            Objects.equals(platform, that.platform) &&
            Objects.equals(processing_status, that.processing_status) &&
            Objects.equals(comments, that.comments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        msisdn,
        imsi,
        platform,
        processing_status,
        comments
        );
    }

    @Override
    public String toString() {
        return "Deleted_subscriberCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (msisdn != null ? "msisdn=" + msisdn + ", " : "") +
                (imsi != null ? "imsi=" + imsi + ", " : "") +
                (platform != null ? "platform=" + platform + ", " : "") +
                (processing_status != null ? "processing_status=" + processing_status + ", " : "") +
                (comments != null ? "comments=" + comments + ", " : "") +
            "}";
    }

}
