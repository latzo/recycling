package free.sn.service;

import free.sn.domain.Exclude_number;

import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

/**
 * Service Interface for managing {@link Exclude_number}.
 */
public interface Exclude_numberService {

    /**
     * Save a exclude_number.
     *
     * @param exclude_number the entity to save.
     * @return the persisted entity.
     */
    Exclude_number save(Exclude_number exclude_number);

    /**
     * Get all the exclude_numbers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Exclude_number> findAll(Pageable pageable);


    /**
     * Get the "id" exclude_number.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Exclude_number> findOne(Long id);

    /**
     * Get the "id" exclude_number.
     *
     * @param msisdn the id of the entity.
     * @return the entity.
     */
    Optional<Exclude_number> findByMsisdn(String msisdn);

    /**
     * Delete the "id" exclude_number.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    String bulk(InputStream inputStream) throws IOException, JSONException;
}
