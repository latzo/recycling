package free.sn.service;

import free.sn.domain.Delete_subscriber;

import free.sn.domain.Delete_subscriber_logs;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

/**
 * Service Interface for managing {@link Delete_subscriber}.
 */
public interface Delete_subscriberService {

    /**
     * Save a delete_subscriber.
     *
     * @param delete_subscriber the entity to save.
     * @return the persisted entity.
     */
    Delete_subscriber save(Delete_subscriber delete_subscriber);

    /**
     * Get all the delete_subscribers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Delete_subscriber> findAll(Pageable pageable);

    Page<Delete_subscriber> findAllByKeyword(String keyword, Pageable pageable);


    /**
     * Get the "id" delete_subscriber.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Delete_subscriber> findOne(Long id);

    /**
     * Delete the "id" delete_subscriber.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    String bulk(InputStream inputStream) throws IOException, JSONException;
}
