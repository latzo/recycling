/*
package free.sn.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import free.sn.domain.Exclude_number;
import free.sn.domain.*; // for static metamodels
import free.sn.repository.Exclude_numberRepository;
import free.sn.service.dto.Exclude_numberCriteria;

*/
/**
 * Service for executing complex queries for {@link Exclude_number} entities in the database.
 * The main input is a {@link Exclude_numberCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Exclude_number} or a {@link Page} of {@link Exclude_number} which fulfills the criteria.
 *//*

@Service
@Transactional(readOnly = true)
public class Exclude_numberQueryService extends QueryService<Exclude_number> {

    private final Logger log = LoggerFactory.getLogger(Exclude_numberQueryService.class);

    private final Exclude_numberRepository exclude_numberRepository;

    public Exclude_numberQueryService(Exclude_numberRepository exclude_numberRepository) {
        this.exclude_numberRepository = exclude_numberRepository;
    }

    */
/**
     * Return a {@link List} of {@link Exclude_number} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     *//*

    @Transactional(readOnly = true)
    public List<Exclude_number> findByCriteria(Exclude_numberCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Exclude_number> specification = createSpecification(criteria);
        return exclude_numberRepository.findAll(specification);
    }

    */
/**
     * Return a {@link Page} of {@link Exclude_number} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     *//*

    @Transactional(readOnly = true)
    public Page<Exclude_number> findByCriteria(Exclude_numberCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Exclude_number> specification = createSpecification(criteria);
        return exclude_numberRepository.findAll(specification, page);
    }

    */
/**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     *//*

    @Transactional(readOnly = true)
    public long countByCriteria(Exclude_numberCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Exclude_number> specification = createSpecification(criteria);
        return exclude_numberRepository.count(specification);
    }

    */
/**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     *//*

    private Specification<Exclude_number> createSpecification(Exclude_numberCriteria criteria) {
        Specification<Exclude_number> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Exclude_number_.id));
            }
            if (criteria.getMsisdn() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMsisdn(), Exclude_number_.msisdn));
            }
            if (criteria.getComments() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComments(), Exclude_number_.comments));
            }
        }
        return specification;
    }
}
*/
