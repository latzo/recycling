package free.sn.service;

import free.sn.domain.Delete_subscriber_logs;
import free.sn.domain.Deleted_subscriber;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Deleted_subscriber}.
 */
public interface Deleted_subscriberService {

    /**
     * Save a deleted_subscriber.
     *
     * @param deleted_subscriber the entity to save.
     * @return the persisted entity.
     */
    Deleted_subscriber save(Deleted_subscriber deleted_subscriber);

    /**
     * Get all the deleted_subscribers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Deleted_subscriber> findAll(Pageable pageable);
    List<Deleted_subscriber> findAllReport();


    /**
     * Get the "id" deleted_subscriber.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Deleted_subscriber> findOne(Long id);

    /**
     * Delete the "id" deleted_subscriber.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<Deleted_subscriber> findBySearch(Pageable pageable, String msisdn, String ismi,String platform, String status, Instant debut, Instant fin);

    Page<Deleted_subscriber> search(Pageable pageable, String msisdn, String ismi, String platform, String status, Instant debut, Instant fin);
    List<Deleted_subscriber> exportSearch(String msisdn, String ismi, String platform, String status, Instant debut, Instant fin);


}
