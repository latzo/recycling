package free.sn.service.impl;

import free.sn.domain.Deleted_subscriber;
import free.sn.domain.Exclude_number;
import free.sn.repository.Exclude_numberRepository;
import free.sn.service.Delete_subscriberService;
import free.sn.domain.Delete_subscriber;
import free.sn.repository.Delete_subscriberRepository;
import free.sn.service.Exclude_numberService;
import free.sn.web.rest.errors.BadRequestAlertException;
import org.junit.jupiter.params.shadow.com.univocity.parsers.csv.CsvParser;
import org.junit.jupiter.params.shadow.com.univocity.parsers.csv.CsvParserSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Service Implementation for managing {@link Delete_subscriber}.
 */
@Service
@Transactional
public class Delete_subscriberServiceImpl implements Delete_subscriberService {

    private final Logger log = LoggerFactory.getLogger(Delete_subscriberServiceImpl.class);

    private final Delete_subscriberRepository delete_subscriberRepository;

    @Autowired
    private Exclude_numberRepository exclude_numberRepository;

    public Delete_subscriberServiceImpl(Delete_subscriberRepository delete_subscriberRepository) {
        this.delete_subscriberRepository = delete_subscriberRepository;
    }

    /**
     * Save a delete_subscriber.
     *
     * @param delete_subscriber the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Delete_subscriber save(Delete_subscriber delete_subscriber) {
        log.debug("Request to save Delete_subscriber : {}", delete_subscriber);
        Optional<Exclude_number> exclude_number = exclude_numberRepository.findByMsisdn(delete_subscriber.getMsisdn());
        return !exclude_number.isPresent()? delete_subscriberRepository.save(delete_subscriber):null;
    }

    /**
     * Get all the delete_subscribers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Delete_subscriber> findAll(Pageable pageable) {
        log.debug("Request to get all Delete_subscribers");
        return delete_subscriberRepository.findAllByOrderByCreatedDateDesc(pageable);
    }

    /**
     * Get all the deleted_subscribers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Delete_subscriber> findAllByKeyword(String keyword, Pageable pageable) {
        log.debug("Request to get all Deleted_subscribers");
        return delete_subscriberRepository.findAllByKeyword(keyword, pageable);
    }

    /**
     * Get one delete_subscriber by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Delete_subscriber> findOne(Long id) {
        log.debug("Request to get Delete_subscriber : {}", id);
        return delete_subscriberRepository.findById(id);
    }

    /**
     * Delete the delete_subscriber by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Delete_subscriber : {}", id);
        delete_subscriberRepository.deleteById(id);
    }


    @Override
    public String bulk(InputStream inputStream) throws IOException, JSONException {
        // SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        CsvParserSettings settings = new CsvParserSettings();
        settings.setLineSeparatorDetectionEnabled(true);
        settings.getFormat().setDelimiter(';');
        CsvParser parser = new CsvParser(settings);
        List<String[]> allLines = parser.parseAll(inputStream);
        JSONObject json = new JSONObject();
        if (allLines.size() > 50000){
            try {
                json.put("key", "Le nombre de lignes doit être < à 50.000");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return json.toString();
        }

        String regexNumber = "\\d+";
        AtomicReference<Boolean> error = new AtomicReference<>(false);
        AtomicInteger n = new AtomicInteger();
        AtomicReference<String> message = new AtomicReference<>("");

        allLines.forEach( l -> {
            n.getAndIncrement();
                l[0] = l[0].toString().startsWith("221") ?
                    l[0].toString().replace("221", "")
                    :
                    l[0];
                //
            if(l[0] == null || l[1] == null || l[2] == null ){
                message.set("Ligne " + n + ": tous les champs sont obligatoires");
                error.set(true);
            }else {


                if (!l[0].toString().matches(regexNumber) || l[0].toString().length() < 9 || l[0].toString().length() > 9) {
                    message.set("Ligne " + n + ": format MSISDN non valide");
                    error.set(true);
                }

                if (!l[1].toString().matches(regexNumber)) {
                    message.set("Ligne " + n + ": format IMSI non valide");
                    error.set(true);
                }
            }

        } );

        if(error.get()){
            try {
                json.put("key", message.get());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        return json.toString();
        } else {
            String trxID = UUID.randomUUID().toString();
            allLines.forEach( l -> {
                l[0] = l[0].toString().startsWith("221") ?
                    l[0].toString().replace("221", "")
                    :
                    l[0];
                Delete_subscriber delete_subscriber = new Delete_subscriber();
                delete_subscriber.setMsisdn(l[0].toString());
                delete_subscriber.setImsi(l[1].toString());
                delete_subscriber.setPlatform(l[2].toString());
                delete_subscriber.setTrxID(trxID);
                //check if msisdn is present
                Optional<Exclude_number> exclude_number = exclude_numberRepository.findByMsisdn(l[0].toString());
                if (!exclude_number.isPresent()){
                    this.save(delete_subscriber);
                }

            });

        }

//        JSONObject json = new JSONObject();
//        Workbook workbook = new XSSFWorkbook(inputStream);
//        Delete_subscriber delete_subscriber = new Delete_subscriber();
//        String msisdn = ""; String ismi = ""; String platform = "";
//        XSSFSheet sheet = (XSSFSheet) workbook.getSheetAt(0);
//        int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
//        Row row1 = sheet.getRow(1);
//        if (row1.getCell(0).toString() == "") {
//            json.put("key", "Ligne 1: le MSISDN ne peut pas être vide");
//            return json.toString();
//        }
//
//        if(row1.getCell(0).toString().length() < 9 || row1.getCell(0).toString().length() > 12 ){
//            json.put("key", "Ligne 1: MSISDN incomplet");
//            return json.toString();
//        }
//
//
//        for (int i = 1; i < rowCount + 1; i++) {
//
//            Row row = sheet.getRow(i);
//
//            if (row.getCell(0).toString() == "" || row.getCell(1).toString() == "" || row.getCell(2).toString() == "") {
//                json.put("key", "Ligne " + i + ": Tous les champs sont obligatoires.");
//                return json.toString();
//            }
//
//
//            if(row.getCell(0).toString().length() < 9 ){
//                json.put("key", "Ligne " + i + ": MSISDN incomplet");
//                return json.toString();
//            }
//
//
//
//
//        }
//
//        //save row 1
//        String msisdn2_row1 = "";
//        try {
//            double msisdn1_row1 = Double.parseDouble(row1.getCell(0).toString().replace(" ", ""));
//            msisdn2_row1 = String.format("%.0f", msisdn1_row1);
//        } catch (Exception e) {
//            json.put("key", "Ligne " + 1 + ": format invalide du MSISDN. Format valide: 760000000 ou 221760000000");
//            return json.toString();
//        }
//        String imsi_row1 = "";
//        try {
//            double imsi2_row1 = Double.parseDouble(row1.getCell(1).toString().replace(" ", ""));
//            imsi_row1 = String.format("%.0f", imsi2_row1);
//        } catch (Exception e) {
//            json.put("key", "Ligne " + 1 + ": format invalide du imsi");
//            return json.toString();
//        }
//
//        delete_subscriber.setMsisdn(msisdn2_row1);
//        delete_subscriber.setImsi(imsi_row1);
//        delete_subscriber.setPlatform(row1.getCell(2).toString());
//        this.save(delete_subscriber);
//        // check if exist
//        // accessListFound = findByCinAndMsisdnContains(Cni2, msisdn2_row1);
//
//
//        // save from row2
//
//        for (int i = 2; i < rowCount + 1; i++) {
//            Row row = sheet.getRow(i);
//            double msisdn_row = Double.parseDouble(row.getCell(0).toString().replace(" ", ""));
//            String msisdn2_row = String.format("%.0f", msisdn_row);
//            double imsi_row = Double.parseDouble(row.getCell(1).toString().replace(" ", ""));
//            String imsi2_row = String.format("%.0f", imsi_row);
//            Delete_subscriber ds = new Delete_subscriber();
//            ds.setMsisdn(msisdn2_row); ds.setImsi(imsi2_row); ds.setPlatform(row.getCell(2).toString());
//            this.save( ds );
//
//        }

        return null;
    }



}
