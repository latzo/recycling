package free.sn.service.impl;

import free.sn.domain.Delete_subscriber_logs;
import free.sn.service.Deleted_subscriberService;
import free.sn.domain.Deleted_subscriber;
import free.sn.repository.Deleted_subscriberRepository;
import io.micrometer.core.instrument.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Service Implementation for managing {@link Deleted_subscriber}.
 */
@Service
@Transactional
public class Deleted_subscriberServiceImpl implements Deleted_subscriberService {

    private final Logger log = LoggerFactory.getLogger(Deleted_subscriberServiceImpl.class);

    private final Deleted_subscriberRepository deleted_subscriberRepository;

    public Deleted_subscriberServiceImpl(Deleted_subscriberRepository deleted_subscriberRepository) {
        this.deleted_subscriberRepository = deleted_subscriberRepository;
    }

    /**
     * Save a deleted_subscriber.
     *
     * @param deleted_subscriber the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Deleted_subscriber save(Deleted_subscriber deleted_subscriber) {
        log.debug("Request to save Deleted_subscriber : {}", deleted_subscriber);
        return deleted_subscriberRepository.save(deleted_subscriber);
    }

    /**
     * Get all the deleted_subscribers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Deleted_subscriber> findAll(Pageable pageable) {
        log.debug("Request to get all Deleted_subscribers");
        return deleted_subscriberRepository.findAll(pageable);
    }

    @Override
    public List<Deleted_subscriber> findAllReport() {
        return deleted_subscriberRepository.findAllReport();
    }


    /**
     * Get one deleted_subscriber by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Deleted_subscriber> findOne(Long id) {
        log.debug("Request to get Deleted_subscriber : {}", id);
        return deleted_subscriberRepository.findById(id);
    }

    /**
     * Delete the deleted_subscriber by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Deleted_subscriber : {}", id);
        deleted_subscriberRepository.deleteById(id);
    }

    @Override
    public List<Deleted_subscriber> findBySearch(Pageable pageable, String msisdn, String imsi, String platform, String status, Instant debut, Instant fin) {
        List<Deleted_subscriber> listDeleted = new ArrayList<>();

        if (debut != null) {
            if(debut.equals(fin)){
                listDeleted.addAll(deleted_subscriberRepository.findByCreatedDateAfter(debut));
            }else{
                listDeleted.addAll(deleted_subscriberRepository.findByCreatedDateBetween(debut, fin));
            }
        }
        else{
            listDeleted.addAll(deleted_subscriberRepository.findByCreatedDateBefore(fin));
        }
//

        Stream<Deleted_subscriber> s = listDeleted.stream()
            .distinct()
            .sorted((l1,l2) -> l2.getCreatedDate().compareTo(l1.getCreatedDate()));
        if (StringUtils.isNotBlank(msisdn)) {
            s = s.filter(deleteSubscriber -> deleteSubscriber.getMsisdn().equalsIgnoreCase(msisdn));
        }
        if (StringUtils.isNotBlank(imsi)) {
            s = s.filter(deleteSubscriber -> deleteSubscriber.getImsi().equalsIgnoreCase(imsi));
        }
        if (StringUtils.isNotBlank(platform)) {
            s = s.filter(deleteSubscriber -> deleteSubscriber.getPlatform().equalsIgnoreCase(platform));
        }
        if (StringUtils.isNotBlank(status)) {
            s = s.filter(deleteSubscriber -> deleteSubscriber.getProcessing_status().equalsIgnoreCase(status));
        }
        return s.collect(Collectors.toList());
    }

    @Override
    public Page<Deleted_subscriber> search(Pageable pageable, String msisdn, String ismi, String platform, String status, Instant debut, Instant fin) {
        System.out.printf("debut\n");
        System.out.println(debut);
        System.out.printf("fin\n");
        System.out.println(fin);
        return deleted_subscriberRepository.search(pageable, msisdn, ismi, platform, status, debut, fin);
    }

    @Override
    public List<Deleted_subscriber> exportSearch(String msisdn, String ismi, String platform, String status, Instant debut, Instant fin) {
        return deleted_subscriberRepository.exportSearch(msisdn, ismi,platform, status, debut, fin);
    }

}
