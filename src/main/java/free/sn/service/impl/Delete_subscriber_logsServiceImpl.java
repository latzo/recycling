package free.sn.service.impl;

import free.sn.domain.Deleted_subscriber;
import free.sn.service.Delete_subscriber_logsService;
import free.sn.domain.Delete_subscriber_logs;
import free.sn.repository.Delete_subscriber_logsRepository;
import io.micrometer.core.instrument.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.chrono.ChronoZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Service Implementation for managing {@link Delete_subscriber_logs}.
 */
@Service
@Transactional
public class Delete_subscriber_logsServiceImpl implements Delete_subscriber_logsService {

    private final Logger log = LoggerFactory.getLogger(Delete_subscriber_logsServiceImpl.class);

    private final Delete_subscriber_logsRepository delete_subscriber_logsRepository;

    public Delete_subscriber_logsServiceImpl(Delete_subscriber_logsRepository delete_subscriber_logsRepository) {
        this.delete_subscriber_logsRepository = delete_subscriber_logsRepository;
    }

    /**
     * Save a delete_subscriber_logs.
     *
     * @param delete_subscriber_logs the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Delete_subscriber_logs save(Delete_subscriber_logs delete_subscriber_logs) {
        log.debug("Request to save Delete_subscriber_logs : {}", delete_subscriber_logs);
        return delete_subscriber_logsRepository.save(delete_subscriber_logs);
    }

    /**
     * Get all the delete_subscriber_logs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Delete_subscriber_logs> findAll(Pageable pageable) {
        log.debug("Request to get all Delete_subscriber_logs");
        return delete_subscriber_logsRepository.findAll(pageable);
    }

    /**
     * Get one delete_subscriber_logs by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Delete_subscriber_logs> findOne(Long id) {
        log.debug("Request to get Delete_subscriber_logs : {}", id);
        return delete_subscriber_logsRepository.findById(id);
    }

    /**
     * Delete the delete_subscriber_logs by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Delete_subscriber_logs : {}", id);
        delete_subscriber_logsRepository.deleteById(id);
    }

    @Override
    public List<Delete_subscriber_logs> findBySearch(Pageable pageable, String msisdn, String imsi, String platform, String status, ZonedDateTime debut, ZonedDateTime fin) {
        List<Delete_subscriber_logs> listDeleted = new ArrayList<>();

        if (debut != null) {
            if(debut.equals(fin)){
                listDeleted.addAll(delete_subscriber_logsRepository.findByDeletedDateAfter(debut));
            }else{
                listDeleted.addAll(delete_subscriber_logsRepository.findByDeletedDateBetween(debut, fin));
            }
        }
        else{
            listDeleted.addAll(delete_subscriber_logsRepository.findByDeletedBefore(fin));
        }
//

        Stream<Delete_subscriber_logs> s = listDeleted.stream()
            .distinct()
            .sorted((l1,l2) -> l2.getDeleted_date().compareTo(l1.getDeleted_date()));
        if (StringUtils.isNotBlank(msisdn)) {
            s = s.filter(deleteSubscriber -> deleteSubscriber.getMsisdn().equalsIgnoreCase(msisdn));
        }
        if (StringUtils.isNotBlank(imsi)) {
            s = s.filter(deleteSubscriber -> deleteSubscriber.getImsi().equalsIgnoreCase(imsi));
        }
        if (StringUtils.isNotBlank(platform)) {
            s = s.filter(deleteSubscriber -> deleteSubscriber.getPlatform().equalsIgnoreCase(platform));
        }
        if (StringUtils.isNotBlank(status)) {
            s = s.filter(deleteSubscriber -> deleteSubscriber.getDeleted_status().equalsIgnoreCase(status));
        }
        return s.collect(Collectors.toList());
    }

    @Override
    public Page<Delete_subscriber_logs> search(Pageable pageable, String msisdn, String ismi, String platform, String status,  ZonedDateTime debut, ZonedDateTime fin) {
        return delete_subscriber_logsRepository.search(pageable, msisdn, ismi, platform, status, debut, fin);
    }
}
