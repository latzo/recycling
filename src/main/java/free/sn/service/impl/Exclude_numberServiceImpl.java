package free.sn.service.impl;

import free.sn.domain.Delete_subscriber;
import free.sn.service.Exclude_numberService;
import free.sn.domain.Exclude_number;
import free.sn.repository.Exclude_numberRepository;
import org.junit.jupiter.params.shadow.com.univocity.parsers.csv.CsvParser;
import org.junit.jupiter.params.shadow.com.univocity.parsers.csv.CsvParserSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Service Implementation for managing {@link Exclude_number}.
 */
@Service
@Transactional
public class Exclude_numberServiceImpl implements Exclude_numberService {

    private final Logger log = LoggerFactory.getLogger(Exclude_numberServiceImpl.class);

    private final Exclude_numberRepository exclude_numberRepository;

    public Exclude_numberServiceImpl(Exclude_numberRepository exclude_numberRepository) {
        this.exclude_numberRepository = exclude_numberRepository;
    }

    /**
     * Save a exclude_number.
     *
     * @param exclude_number the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Exclude_number save(Exclude_number exclude_number) {
        log.debug("Request to save Exclude_number : {}", exclude_number);
        return exclude_numberRepository.save(exclude_number);
    }

    /**
     * Get all the exclude_numbers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Exclude_number> findAll(Pageable pageable) {
        log.debug("Request to get all Exclude_numbers");
        return exclude_numberRepository.findAll(pageable);
    }


    /**
     * Get one exclude_number by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Exclude_number> findOne(Long id) {
        log.debug("Request to get Exclude_number : {}", id);
        return exclude_numberRepository.findById(id);
    }

    @Override
    public Optional<Exclude_number> findByMsisdn(String msisdn) {
        return exclude_numberRepository.findByMsisdn(msisdn);
    }

    /**
     * Delete the exclude_number by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Exclude_number : {}", id);
        exclude_numberRepository.deleteById(id);
    }




    @Override
    public String bulk(InputStream inputStream) throws IOException, JSONException {
        // SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        CsvParserSettings settings = new CsvParserSettings();
        settings.setLineSeparatorDetectionEnabled(true);
        settings.getFormat().setDelimiter(';');
        CsvParser parser = new CsvParser(settings);
        List<String[]> allLines = parser.parseAll(inputStream);
        JSONObject json = new JSONObject();
        System.out.printf("check regex\n");

        String regexNumber = "\\d+";
        AtomicReference<Boolean> error = new AtomicReference<>(false);
        AtomicInteger n = new AtomicInteger();
        AtomicReference<String> message = new AtomicReference<>("");

        allLines.forEach(l -> {
            n.getAndIncrement();
            l[0] = l[0].toString().startsWith("221") ?
                l[0].toString().replace("221", "")
                :
                l[0];
            //
            if (l[0] == null ) {
                message.set("Ligne " + n + ": le msisdn est obligatoire");
                error.set(true);
            } else {


                if (!l[0].toString().matches(regexNumber) || l[0].toString().length() < 9 || l[0].toString().length() > 9) {
                    message.set("Ligne " + n + ": format MSISDN non valide");
                    error.set(true);
                }


            }

        });

        if (error.get()) {
            try {
                json.put("key", message.get());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return json.toString();
        } else {
            allLines.forEach(l -> {
                l[0] = l[0].toString().startsWith("221") ?
                    l[0].toString().replace("221", "")
                    :
                    l[0];
                Exclude_number exclude_number = new Exclude_number();
                exclude_number.setMsisdn(l[0].toString());
                exclude_number.setComments(l[1].toString());

                this.save(exclude_number);

            });

        }
        return null;
    }
}
