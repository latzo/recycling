package free.sn.security;



import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Slf4j
@Getter

public enum TertioRoles {
    ADMIN("ADMIN", "ROLE_ADMIN"),
    USER("USER", "ROLE_USER"),
    ITPRODUCT("G_IT_PRODUCT", "ROLE_G_IT_PRODUCT"),
    ITOPS("IT_Ops", "ROLE_IT_Ops"),
    DT_IN("DT IN", "ROLE_DT_IN");

    public static List<TertioRoles> allRoles;

    static {
        List<TertioRoles> roles = new ArrayList<>(Arrays.asList(TertioRoles.values()));
        allRoles = Collections.unmodifiableList(roles);
    }

    private String activeDirectoryGroup;
    private String role;
    private GrantedAuthority authority;


    TertioRoles(String activeDirectoryGroup, String role) {
        this.activeDirectoryGroup = activeDirectoryGroup;
        this.role = role;
        this.authority = new SimpleGrantedAuthority(activeDirectoryGroup);
    }

}
