package free.sn.web.rest;

import free.sn.domain.Exclude_number;
import free.sn.service.Exclude_numberService;
import free.sn.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing {@link free.sn.domain.Exclude_number}.
 */
@RestController
@RequestMapping("/api")
public class Exclude_numberResource {

    private final Logger log = LoggerFactory.getLogger(Exclude_numberResource.class);

    private static final String ENTITY_NAME = "exclude_number";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final Exclude_numberService exclude_numberService;

    public Exclude_numberResource(Exclude_numberService exclude_numberService) {
        this.exclude_numberService = exclude_numberService;
    }

    /**
     * {@code POST  /exclude-numbers} : Create a new exclude_number.
     *
     * @param exclude_number the exclude_number to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new exclude_number, or with status {@code 400 (Bad Request)} if the exclude_number has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/exclude-numbers")
    public ResponseEntity<Exclude_number> createExclude_number(@RequestBody Exclude_number exclude_number) throws URISyntaxException {
        log.debug("REST request to save Exclude_number : {}", exclude_number);
        if (exclude_number.getId() != null) {
            throw new BadRequestAlertException("A new exclude_number cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Exclude_number result = exclude_numberService.save(exclude_number);
        return ResponseEntity.created(new URI("/api/exclude-numbers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /exclude-numbers} : Updates an existing exclude_number.
     *
     * @param exclude_number the exclude_number to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated exclude_number,
     * or with status {@code 400 (Bad Request)} if the exclude_number is not valid,
     * or with status {@code 500 (Internal Server Error)} if the exclude_number couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/exclude-numbers")
    public ResponseEntity<Exclude_number> updateExclude_number(@RequestBody Exclude_number exclude_number) throws URISyntaxException {
        log.debug("REST request to update Exclude_number : {}", exclude_number);
        if (exclude_number.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Exclude_number result = exclude_numberService.save(exclude_number);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, exclude_number.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /exclude-numbers} : get all the exclude_numbers.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of exclude_numbers in body.
     */
    @GetMapping("/exclude-numbers")
    public ResponseEntity<List<Exclude_number>> getAllExclude_numbers(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of Exclude_numbers");
        Page<Exclude_number> page = exclude_numberService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /exclude-numbers/:id} : get the "id" exclude_number.
     *
     * @param id the id of the exclude_number to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the exclude_number, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/exclude-numbers/{id}")
    public ResponseEntity<Exclude_number> getExclude_number(@PathVariable Long id) {
        log.debug("REST request to get Exclude_number : {}", id);
        Optional<Exclude_number> exclude_number = exclude_numberService.findOne(id);
        return ResponseUtil.wrapOrNotFound(exclude_number);
    }



    /**
     * {@code DELETE  /exclude-numbers/:id} : delete the "id" exclude_number.
     *
     * @param id the id of the exclude_number to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/exclude-numbers/{id}")
    public ResponseEntity<Void> deleteExclude_number(@PathVariable Long id) {
        log.debug("REST request to delete Exclude_number : {}", id);
        exclude_numberService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/exclude-numbers/upload")
    public String bulk(@RequestParam("file") MultipartFile file) throws IOException, URISyntaxException, JSONException {
        String txId = UUID.randomUUID().toString();
        this.log.debug(" bulking this file {} with transactionID {}", file);
        String result = this.exclude_numberService.bulk(file.getInputStream());
        return result;

    }
}
