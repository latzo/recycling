package free.sn.web.rest;

import free.sn.domain.Delete_subscriber_logs;
import free.sn.domain.Deleted_subscriber;
import free.sn.service.Delete_subscriber_logsService;
import free.sn.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.micrometer.core.instrument.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link free.sn.domain.Delete_subscriber_logs}.
 */
@RestController
@RequestMapping("/api")
public class Delete_subscriber_logsResource {

    private final Logger log = LoggerFactory.getLogger(Delete_subscriber_logsResource.class);

    private static final String ENTITY_NAME = "delete_subscriber_logs";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final Delete_subscriber_logsService delete_subscriber_logsService;

    public Delete_subscriber_logsResource(Delete_subscriber_logsService delete_subscriber_logsService) {
        this.delete_subscriber_logsService = delete_subscriber_logsService;
    }

    /**
     * {@code POST  /delete-subscriber-logs} : Create a new delete_subscriber_logs.
     *
     * @param delete_subscriber_logs the delete_subscriber_logs to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new delete_subscriber_logs, or with status {@code 400 (Bad Request)} if the delete_subscriber_logs has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/delete-subscriber-logs")
    public ResponseEntity<Delete_subscriber_logs> createDelete_subscriber_logs(@RequestBody Delete_subscriber_logs delete_subscriber_logs) throws URISyntaxException {
        log.debug("REST request to save Delete_subscriber_logs : {}", delete_subscriber_logs);
        if (delete_subscriber_logs.getId() != null) {
            throw new BadRequestAlertException("A new delete_subscriber_logs cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Delete_subscriber_logs result = delete_subscriber_logsService.save(delete_subscriber_logs);
        return ResponseEntity.created(new URI("/api/delete-subscriber-logs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /delete-subscriber-logs} : Updates an existing delete_subscriber_logs.
     *
     * @param delete_subscriber_logs the delete_subscriber_logs to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated delete_subscriber_logs,
     * or with status {@code 400 (Bad Request)} if the delete_subscriber_logs is not valid,
     * or with status {@code 500 (Internal Server Error)} if the delete_subscriber_logs couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/delete-subscriber-logs")
    public ResponseEntity<Delete_subscriber_logs> updateDelete_subscriber_logs(@RequestBody Delete_subscriber_logs delete_subscriber_logs) throws URISyntaxException {
        log.debug("REST request to update Delete_subscriber_logs : {}", delete_subscriber_logs);
        if (delete_subscriber_logs.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Delete_subscriber_logs result = delete_subscriber_logsService.save(delete_subscriber_logs);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, delete_subscriber_logs.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /delete-subscriber-logs} : get all the delete_subscriber_logs.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of delete_subscriber_logs in body.
     */
    @GetMapping("/delete-subscriber-logs")
    public ResponseEntity<List<Delete_subscriber_logs>> getAllDelete_subscriber_logs(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of Delete_subscriber_logs");
            Page<Delete_subscriber_logs> page = delete_subscriber_logsService.findAll(pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
            return ResponseEntity.ok().headers(headers).body(page.getContent());

    }

    /**
     * {@code GET  /delete-subscriber-logs/:id} : get the "id" delete_subscriber_logs.
     *
     * @param id the id of the delete_subscriber_logs to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the delete_subscriber_logs, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/delete-subscriber-logs/{id}")
    public ResponseEntity<Delete_subscriber_logs> getDelete_subscriber_logs(@PathVariable Long id) {
        log.debug("REST request to get Delete_subscriber_logs : {}", id);
        Optional<Delete_subscriber_logs> delete_subscriber_logs = delete_subscriber_logsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(delete_subscriber_logs);
    }

    /**
     * {@code DELETE  /delete-subscriber-logs/:id} : delete the "id" delete_subscriber_logs.
     *
     * @param id the id of the delete_subscriber_logs to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/delete-subscriber-logs/{id}")
    public ResponseEntity<Void> deleteDelete_subscriber_logs(@PathVariable Long id) {
        log.debug("REST request to delete Delete_subscriber_logs : {}", id);
        delete_subscriber_logsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code GET  /deleted-subscribers} : get all the deleted_subscribers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of deleted_subscribers in body.
     */
    @GetMapping("/delete-subscriber-logs/search")
    public ResponseEntity<List<Delete_subscriber_logs>> getDeletedBySearch(Pageable pageable, @RequestParam (required = false) String msisdn,@RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder,
                                                                       @RequestParam (required = false) String imsi, @RequestParam (required = false) String platform, @RequestParam (required = false) String status,
                                                                       @RequestParam (required = false) String debut, @RequestParam (required = false) String fin) {
        //+ "T00:00:00.00Z"
        ZonedDateTime debutt = !StringUtils.isBlank(debut) ? ZonedDateTime.parse(debut) : null;
        ZonedDateTime finn = !StringUtils.isBlank(fin) ? ZonedDateTime.parse(fin) : ZonedDateTime.now();
        Page<Delete_subscriber_logs> page = delete_subscriber_logsService.search(pageable, msisdn, imsi, platform, status, debutt, finn);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
