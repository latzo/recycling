package free.sn.web.rest;

import free.sn.domain.Delete_subscriber_logs;
import free.sn.domain.Deleted_subscriber;
import free.sn.service.Deleted_subscriberService;
import free.sn.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.micrometer.core.instrument.util.StringUtils;
import io.swagger.models.auth.In;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link free.sn.domain.Deleted_subscriber}.
 */
@RestController
@RequestMapping("/api")
public class Deleted_subscriberResource {

    private final Logger log = LoggerFactory.getLogger(Deleted_subscriberResource.class);

    private static final String ENTITY_NAME = "deleted_subscriber";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final Deleted_subscriberService deleted_subscriberService;

    public Deleted_subscriberResource(Deleted_subscriberService deleted_subscriberService) {
        this.deleted_subscriberService = deleted_subscriberService;
    }

    /**
     * {@code POST  /deleted-subscribers} : Create a new deleted_subscriber.
     *
     * @param deleted_subscriber the deleted_subscriber to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new deleted_subscriber, or with status {@code 400 (Bad Request)} if the deleted_subscriber has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/deleted-subscribers")
    public ResponseEntity<Deleted_subscriber> createDeleted_subscriber(@RequestBody Deleted_subscriber deleted_subscriber) throws URISyntaxException {
        log.debug("REST request to save Deleted_subscriber : {}", deleted_subscriber);
        if (deleted_subscriber.getId() != null) {
            throw new BadRequestAlertException("A new deleted_subscriber cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Deleted_subscriber result = deleted_subscriberService.save(deleted_subscriber);
        return ResponseEntity.created(new URI("/api/deleted-subscribers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /deleted-subscribers} : Updates an existing deleted_subscriber.
     *
     * @param deleted_subscriber the deleted_subscriber to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated deleted_subscriber,
     * or with status {@code 400 (Bad Request)} if the deleted_subscriber is not valid,
     * or with status {@code 500 (Internal Server Error)} if the deleted_subscriber couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/deleted-subscribers")
    public ResponseEntity<Deleted_subscriber> updateDeleted_subscriber(@RequestBody Deleted_subscriber deleted_subscriber) throws URISyntaxException {
        log.debug("REST request to update Deleted_subscriber : {}", deleted_subscriber);
        if (deleted_subscriber.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Deleted_subscriber result = deleted_subscriberService.save(deleted_subscriber);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, deleted_subscriber.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /deleted-subscribers} : get all the deleted_subscribers.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of deleted_subscribers in body.
     */
    @GetMapping("/deleted-subscribers")
    public ResponseEntity<List<Deleted_subscriber>> getAllDeleted_subscribers(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of Deleted_subscribers");
        Page<Deleted_subscriber> page = deleted_subscriberService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /deleted-subscribers/:id} : get the "id" deleted_subscriber.
     *
     * @param id the id of the deleted_subscriber to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the deleted_subscriber, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/deleted-subscribers/{id}")
    public ResponseEntity<Deleted_subscriber> getDeleted_subscriber(@PathVariable Long id) {
        log.debug("REST request to get Deleted_subscriber : {}", id);
        Optional<Deleted_subscriber> deleted_subscriber = deleted_subscriberService.findOne(id);
        return ResponseUtil.wrapOrNotFound(deleted_subscriber);
    }

    /**
     * {@code DELETE  /deleted-subscribers/:id} : delete the "id" deleted_subscriber.
     *
     * @param id the id of the deleted_subscriber to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/deleted-subscribers/{id}")
    public ResponseEntity<Void> deleteDeleted_subscriber(@PathVariable Long id) {
        log.debug("REST request to delete Deleted_subscriber : {}", id);
        deleted_subscriberService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code GET  /deleted-subscribers} : get all the deleted_subscribers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of deleted_subscribers in body.
     */
    @GetMapping("/deleted-subscribers/search")
    public ResponseEntity<List<Deleted_subscriber>> getDeletedBySearch(Pageable pageable, @RequestParam (required = false) String msisdn, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder,
                                                                           @RequestParam (required = false) String imsi, @RequestParam (required = false) String platform, @RequestParam (required = false) String status,
                                                                           @RequestParam (required = false) String debut, @RequestParam (required = false) String fin) {
        //+ "T00:00:00.00Z"
        Instant debutt = !StringUtils.isBlank(debut) ? Instant.parse(debut) : null;
        Instant finn = !StringUtils.isBlank(fin) ? Instant.parse(fin) : Instant.now();
        Page<Deleted_subscriber> page = deleted_subscriberService.search(pageable, msisdn, imsi, platform, status, debutt, finn);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /deleted-subscribers} : get all the deleted_subscribers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of deleted_subscribers in body.
     */
    @GetMapping("/deleted-subscribers/reportAll")
    public ResponseEntity<List<Deleted_subscriber>> reportAll(Pageable pageable) {
        //+ "T00:00:00.00Z"

        List<Deleted_subscriber> page = deleted_subscriberService.findAllReport();
        return ResponseEntity.ok(page);
    }

    @GetMapping("/deleted-subscribers/reportAll/search")
    public ResponseEntity<List<Deleted_subscriber>> reportAllSearch(@RequestParam (required = false) String msisdn, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder,
                                                                       @RequestParam (required = false) String imsi, @RequestParam (required = false) String platform, @RequestParam (required = false) String status,
                                                                       @RequestParam (required = false) String debut, @RequestParam (required = false) String fin) {
        //+ "T00:00:00.00Z"
        Instant debutt = !StringUtils.isBlank(debut) ? Instant.parse(debut) : null;
        Instant finn = !StringUtils.isBlank(fin) ? Instant.parse(fin) : Instant.now();
        List<Deleted_subscriber> page = deleted_subscriberService.exportSearch(msisdn, imsi, platform, status, debutt, finn);
        return ResponseEntity.ok(page);
    }

}
