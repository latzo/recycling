package free.sn.web.rest;

import free.sn.domain.Delete_subscriber;
import free.sn.domain.Deleted_subscriber;
import free.sn.service.Delete_subscriberService;
import free.sn.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing {@link free.sn.domain.Delete_subscriber}.
 */
@RestController
@RequestMapping("/api")
public class Delete_subscriberResource {

    private final Logger log = LoggerFactory.getLogger(Delete_subscriberResource.class);

    private static final String ENTITY_NAME = "delete_subscriber";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final Delete_subscriberService delete_subscriberService;

    public Delete_subscriberResource(Delete_subscriberService delete_subscriberService) {
        this.delete_subscriberService = delete_subscriberService;
    }

    /**
     * {@code POST  /delete-subscribers} : Create a new delete_subscriber.
     *
     * @param delete_subscriber the delete_subscriber to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new delete_subscriber, or with status {@code 400 (Bad Request)} if the delete_subscriber has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/delete-subscribers")
    public ResponseEntity<Delete_subscriber> createDelete_subscriber(@RequestBody Delete_subscriber delete_subscriber) throws URISyntaxException {
        log.debug("REST request to save Delete_subscriber : {}", delete_subscriber);
        if (delete_subscriber.getId() != null) {
            throw new BadRequestAlertException("A new delete_subscriber cannot already have an ID", ENTITY_NAME, "idexists");
        }

        Delete_subscriber result = delete_subscriberService.save(delete_subscriber);

        return result != null ? ResponseEntity.created(new URI("/api/delete-subscribers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result):null;
    }

    /**
     * {@code PUT  /delete-subscribers} : Updates an existing delete_subscriber.
     *
     * @param delete_subscriber the delete_subscriber to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated delete_subscriber,
     * or with status {@code 400 (Bad Request)} if the delete_subscriber is not valid,
     * or with status {@code 500 (Internal Server Error)} if the delete_subscriber couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/delete-subscribers")
    public ResponseEntity<Delete_subscriber> updateDelete_subscriber(@RequestBody Delete_subscriber delete_subscriber) throws URISyntaxException {
        log.debug("REST request to update Delete_subscriber : {}", delete_subscriber);
        if (delete_subscriber.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Delete_subscriber result = delete_subscriberService.save(delete_subscriber);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, delete_subscriber.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /delete-subscribers} : get all the delete_subscribers.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of delete_subscribers in body.
     */
    @GetMapping("/delete-subscribers")
    public ResponseEntity<List<Delete_subscriber>> getAllDelete_subscribers(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of Delete_subscribers");
        if (String.valueOf(queryParams).contains("undefined")){
            Page<Delete_subscriber> page = delete_subscriberService.findAll(pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
            return ResponseEntity.ok().headers(headers).body(page.getContent());
        }else{
            String keyword = String.valueOf(queryParams.get("keyword")).replace("[", "").replace("]", "");
            Page<Delete_subscriber> page = delete_subscriberService.findAllByKeyword("%"+keyword+"%", pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
            return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
    }

    /**
     * {@code GET  /delete-subscribers/:id} : get the "id" delete_subscriber.
     *
     * @param id the id of the delete_subscriber to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the delete_subscriber, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/delete-subscribers/{id}")
    public ResponseEntity<Delete_subscriber> getDelete_subscriber(@PathVariable Long id) {
        log.debug("REST request to get Delete_subscriber : {}", id);
        Optional<Delete_subscriber> delete_subscriber = delete_subscriberService.findOne(id);
        return ResponseUtil.wrapOrNotFound(delete_subscriber);
    }

    /**
     * {@code DELETE  /delete-subscribers/:id} : delete the "id" delete_subscriber.
     *
     * @param id the id of the delete_subscriber to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/delete-subscribers/{id}")
    public ResponseEntity<Void> deleteDelete_subscriber(@PathVariable Long id) {
        log.debug("REST request to delete Delete_subscriber : {}", id);
        delete_subscriberService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/delete-subscribers/upload")
    public String bulk(@RequestParam("file") MultipartFile file) throws IOException, URISyntaxException, JSONException {
        String txId = UUID.randomUUID().toString();
        this.log.debug(" bulking this file {} with transactionID {}", file);
        String result = this.delete_subscriberService.bulk(file.getInputStream());
        return result;

    }



}
