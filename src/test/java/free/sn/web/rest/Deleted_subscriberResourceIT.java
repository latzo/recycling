package free.sn.web.rest;

import free.sn.RecyclingApp;
import free.sn.domain.Deleted_subscriber;
import free.sn.repository.Deleted_subscriberRepository;
import free.sn.service.Deleted_subscriberService;
import free.sn.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static free.sn.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link Deleted_subscriberResource} REST controller.
 */
@SpringBootTest(classes = RecyclingApp.class)
public class Deleted_subscriberResourceIT {
//
//    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
//    private static final String UPDATED_MSISDN = "BBBBBBBBBB";
//
//    private static final String DEFAULT_IMSI = "AAAAAAAAAA";
//    private static final String UPDATED_IMSI = "BBBBBBBBBB";
//
//    private static final String DEFAULT_PLATFORM = "AAAAAAAAAA";
//    private static final String UPDATED_PLATFORM = "BBBBBBBBBB";
//
//    private static final String DEFAULT_PROCESSING_STATUS = "AAAAAAAAAA";
//    private static final String UPDATED_PROCESSING_STATUS = "BBBBBBBBBB";
//
//    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
//    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";
//
//    @Autowired
//    private Deleted_subscriberRepository deleted_subscriberRepository;
//
//    @Autowired
//    private Deleted_subscriberService deleted_subscriberService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restDeleted_subscriberMockMvc;
//
//    private Deleted_subscriber deleted_subscriber;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final Deleted_subscriberResource deleted_subscriberResource = new Deleted_subscriberResource(deleted_subscriberService);
//        this.restDeleted_subscriberMockMvc = MockMvcBuilders.standaloneSetup(deleted_subscriberResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static Deleted_subscriber createEntity(EntityManager em) {
//        Deleted_subscriber deleted_subscriber = new Deleted_subscriber()
//            .msisdn(DEFAULT_MSISDN)
//            .imsi(DEFAULT_IMSI)
//            .platform(DEFAULT_PLATFORM)
//            .processing_status(DEFAULT_PROCESSING_STATUS)
//            .comments(DEFAULT_COMMENTS);
//        return deleted_subscriber;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static Deleted_subscriber createUpdatedEntity(EntityManager em) {
//        Deleted_subscriber deleted_subscriber = new Deleted_subscriber()
//            .msisdn(UPDATED_MSISDN)
//            .imsi(UPDATED_IMSI)
//            .platform(UPDATED_PLATFORM)
//            .processing_status(UPDATED_PROCESSING_STATUS)
//            .comments(UPDATED_COMMENTS);
//        return deleted_subscriber;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        deleted_subscriber = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createDeleted_subscriber() throws Exception {
//        int databaseSizeBeforeCreate = deleted_subscriberRepository.findAll().size();
//
//        // Create the Deleted_subscriber
//        restDeleted_subscriberMockMvc.perform(post("/api/deleted-subscribers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(deleted_subscriber)))
//            .andExpect(status().isCreated());
//
//        // Validate the Deleted_subscriber in the database
//        List<Deleted_subscriber> deleted_subscriberList = deleted_subscriberRepository.findAll();
//        assertThat(deleted_subscriberList).hasSize(databaseSizeBeforeCreate + 1);
//        Deleted_subscriber testDeleted_subscriber = deleted_subscriberList.get(deleted_subscriberList.size() - 1);
//        assertThat(testDeleted_subscriber.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
//        assertThat(testDeleted_subscriber.getImsi()).isEqualTo(DEFAULT_IMSI);
//        assertThat(testDeleted_subscriber.getPlatform()).isEqualTo(DEFAULT_PLATFORM);
//        assertThat(testDeleted_subscriber.getProcessing_status()).isEqualTo(DEFAULT_PROCESSING_STATUS);
//        assertThat(testDeleted_subscriber.getComments()).isEqualTo(DEFAULT_COMMENTS);
//    }
//
//    @Test
//    @Transactional
//    public void createDeleted_subscriberWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = deleted_subscriberRepository.findAll().size();
//
//        // Create the Deleted_subscriber with an existing ID
//        deleted_subscriber.setId(1L);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restDeleted_subscriberMockMvc.perform(post("/api/deleted-subscribers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(deleted_subscriber)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the Deleted_subscriber in the database
//        List<Deleted_subscriber> deleted_subscriberList = deleted_subscriberRepository.findAll();
//        assertThat(deleted_subscriberList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void getAllDeleted_subscribers() throws Exception {
//        // Initialize the database
//        deleted_subscriberRepository.saveAndFlush(deleted_subscriber);
//
//        // Get all the deleted_subscriberList
//        restDeleted_subscriberMockMvc.perform(get("/api/deleted-subscribers?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(deleted_subscriber.getId().intValue())))
//            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
//            .andExpect(jsonPath("$.[*].imsi").value(hasItem(DEFAULT_IMSI.toString())))
//            .andExpect(jsonPath("$.[*].platform").value(hasItem(DEFAULT_PLATFORM.toString())))
//            .andExpect(jsonPath("$.[*].processing_status").value(hasItem(DEFAULT_PROCESSING_STATUS.toString())))
//            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS.toString())));
//    }
//
//    @Test
//    @Transactional
//    public void getDeleted_subscriber() throws Exception {
//        // Initialize the database
//        deleted_subscriberRepository.saveAndFlush(deleted_subscriber);
//
//        // Get the deleted_subscriber
//        restDeleted_subscriberMockMvc.perform(get("/api/deleted-subscribers/{id}", deleted_subscriber.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(deleted_subscriber.getId().intValue()))
//            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
//            .andExpect(jsonPath("$.imsi").value(DEFAULT_IMSI.toString()))
//            .andExpect(jsonPath("$.platform").value(DEFAULT_PLATFORM.toString()))
//            .andExpect(jsonPath("$.processing_status").value(DEFAULT_PROCESSING_STATUS.toString()))
//            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS.toString()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingDeleted_subscriber() throws Exception {
//        // Get the deleted_subscriber
//        restDeleted_subscriberMockMvc.perform(get("/api/deleted-subscribers/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateDeleted_subscriber() throws Exception {
//        // Initialize the database
//        deleted_subscriberService.save(deleted_subscriber);
//
//        int databaseSizeBeforeUpdate = deleted_subscriberRepository.findAll().size();
//
//        // Update the deleted_subscriber
//        Deleted_subscriber updatedDeleted_subscriber = deleted_subscriberRepository.findById(deleted_subscriber.getId()).get();
//        // Disconnect from session so that the updates on updatedDeleted_subscriber are not directly saved in db
//        em.detach(updatedDeleted_subscriber);
//        updatedDeleted_subscriber
//            .msisdn(UPDATED_MSISDN)
//            .imsi(UPDATED_IMSI)
//            .platform(UPDATED_PLATFORM)
//            .processing_status(UPDATED_PROCESSING_STATUS)
//            .comments(UPDATED_COMMENTS);
//
//        restDeleted_subscriberMockMvc.perform(put("/api/deleted-subscribers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(updatedDeleted_subscriber)))
//            .andExpect(status().isOk());
//
//        // Validate the Deleted_subscriber in the database
//        List<Deleted_subscriber> deleted_subscriberList = deleted_subscriberRepository.findAll();
//        assertThat(deleted_subscriberList).hasSize(databaseSizeBeforeUpdate);
//        Deleted_subscriber testDeleted_subscriber = deleted_subscriberList.get(deleted_subscriberList.size() - 1);
//        assertThat(testDeleted_subscriber.getMsisdn()).isEqualTo(UPDATED_MSISDN);
//        assertThat(testDeleted_subscriber.getImsi()).isEqualTo(UPDATED_IMSI);
//        assertThat(testDeleted_subscriber.getPlatform()).isEqualTo(UPDATED_PLATFORM);
//        assertThat(testDeleted_subscriber.getProcessing_status()).isEqualTo(UPDATED_PROCESSING_STATUS);
//        assertThat(testDeleted_subscriber.getComments()).isEqualTo(UPDATED_COMMENTS);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingDeleted_subscriber() throws Exception {
//        int databaseSizeBeforeUpdate = deleted_subscriberRepository.findAll().size();
//
//        // Create the Deleted_subscriber
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restDeleted_subscriberMockMvc.perform(put("/api/deleted-subscribers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(deleted_subscriber)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the Deleted_subscriber in the database
//        List<Deleted_subscriber> deleted_subscriberList = deleted_subscriberRepository.findAll();
//        assertThat(deleted_subscriberList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteDeleted_subscriber() throws Exception {
//        // Initialize the database
//        deleted_subscriberService.save(deleted_subscriber);
//
//        int databaseSizeBeforeDelete = deleted_subscriberRepository.findAll().size();
//
//        // Delete the deleted_subscriber
//        restDeleted_subscriberMockMvc.perform(delete("/api/deleted-subscribers/{id}", deleted_subscriber.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<Deleted_subscriber> deleted_subscriberList = deleted_subscriberRepository.findAll();
//        assertThat(deleted_subscriberList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(Deleted_subscriber.class);
//        Deleted_subscriber deleted_subscriber1 = new Deleted_subscriber();
//        deleted_subscriber1.setId(1L);
//        Deleted_subscriber deleted_subscriber2 = new Deleted_subscriber();
//        deleted_subscriber2.setId(deleted_subscriber1.getId());
//        assertThat(deleted_subscriber1).isEqualTo(deleted_subscriber2);
//        deleted_subscriber2.setId(2L);
//        assertThat(deleted_subscriber1).isNotEqualTo(deleted_subscriber2);
//        deleted_subscriber1.setId(null);
//        assertThat(deleted_subscriber1).isNotEqualTo(deleted_subscriber2);
//    }
}
