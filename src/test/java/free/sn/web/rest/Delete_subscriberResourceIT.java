package free.sn.web.rest;

import free.sn.RecyclingApp;
import free.sn.domain.Delete_subscriber;
import free.sn.repository.Delete_subscriberRepository;
import free.sn.service.Delete_subscriberService;
import free.sn.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static free.sn.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link Delete_subscriberResource} REST controller.
 */
@SpringBootTest(classes = RecyclingApp.class)
public class Delete_subscriberResourceIT {

    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_IMSI = "AAAAAAAAAA";
    private static final String UPDATED_IMSI = "BBBBBBBBBB";

    private static final String DEFAULT_PLATFORM = "AAAAAAAAAA";
    private static final String UPDATED_PLATFORM = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";

    @Autowired
    private Delete_subscriberRepository delete_subscriberRepository;

    @Autowired
    private Delete_subscriberService delete_subscriberService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDelete_subscriberMockMvc;

    private Delete_subscriber delete_subscriber;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final Delete_subscriberResource delete_subscriberResource = new Delete_subscriberResource(delete_subscriberService);
        this.restDelete_subscriberMockMvc = MockMvcBuilders.standaloneSetup(delete_subscriberResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Delete_subscriber createEntity(EntityManager em) {
        Delete_subscriber delete_subscriber = new Delete_subscriber()
            .msisdn(DEFAULT_MSISDN)
            .imsi(DEFAULT_IMSI)
            .platform(DEFAULT_PLATFORM)
            .comments(DEFAULT_COMMENTS);
        return delete_subscriber;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Delete_subscriber createUpdatedEntity(EntityManager em) {
        Delete_subscriber delete_subscriber = new Delete_subscriber()
            .msisdn(UPDATED_MSISDN)
            .imsi(UPDATED_IMSI)
            .platform(UPDATED_PLATFORM)
            .comments(UPDATED_COMMENTS);
        return delete_subscriber;
    }

    @BeforeEach
    public void initTest() {
        delete_subscriber = createEntity(em);
    }

    @Test
    @Transactional
    public void createDelete_subscriber() throws Exception {
        int databaseSizeBeforeCreate = delete_subscriberRepository.findAll().size();

        // Create the Delete_subscriber
        restDelete_subscriberMockMvc.perform(post("/api/delete-subscribers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(delete_subscriber)))
            .andExpect(status().isCreated());

        // Validate the Delete_subscriber in the database
        List<Delete_subscriber> delete_subscriberList = delete_subscriberRepository.findAll();
        assertThat(delete_subscriberList).hasSize(databaseSizeBeforeCreate + 1);
        Delete_subscriber testDelete_subscriber = delete_subscriberList.get(delete_subscriberList.size() - 1);
        assertThat(testDelete_subscriber.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
        assertThat(testDelete_subscriber.getImsi()).isEqualTo(DEFAULT_IMSI);
        assertThat(testDelete_subscriber.getPlatform()).isEqualTo(DEFAULT_PLATFORM);
        assertThat(testDelete_subscriber.getComments()).isEqualTo(DEFAULT_COMMENTS);
    }

    @Test
    @Transactional
    public void createDelete_subscriberWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = delete_subscriberRepository.findAll().size();

        // Create the Delete_subscriber with an existing ID
        delete_subscriber.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDelete_subscriberMockMvc.perform(post("/api/delete-subscribers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(delete_subscriber)))
            .andExpect(status().isBadRequest());

        // Validate the Delete_subscriber in the database
        List<Delete_subscriber> delete_subscriberList = delete_subscriberRepository.findAll();
        assertThat(delete_subscriberList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDelete_subscribers() throws Exception {
        // Initialize the database
        delete_subscriberRepository.saveAndFlush(delete_subscriber);

        // Get all the delete_subscriberList
        restDelete_subscriberMockMvc.perform(get("/api/delete-subscribers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(delete_subscriber.getId().intValue())))
            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
            .andExpect(jsonPath("$.[*].imsi").value(hasItem(DEFAULT_IMSI.toString())))
            .andExpect(jsonPath("$.[*].platform").value(hasItem(DEFAULT_PLATFORM.toString())))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS.toString())));
    }
    
    @Test
    @Transactional
    public void getDelete_subscriber() throws Exception {
        // Initialize the database
        delete_subscriberRepository.saveAndFlush(delete_subscriber);

        // Get the delete_subscriber
        restDelete_subscriberMockMvc.perform(get("/api/delete-subscribers/{id}", delete_subscriber.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(delete_subscriber.getId().intValue()))
            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
            .andExpect(jsonPath("$.imsi").value(DEFAULT_IMSI.toString()))
            .andExpect(jsonPath("$.platform").value(DEFAULT_PLATFORM.toString()))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDelete_subscriber() throws Exception {
        // Get the delete_subscriber
        restDelete_subscriberMockMvc.perform(get("/api/delete-subscribers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDelete_subscriber() throws Exception {
        // Initialize the database
        delete_subscriberService.save(delete_subscriber);

        int databaseSizeBeforeUpdate = delete_subscriberRepository.findAll().size();

        // Update the delete_subscriber
        Delete_subscriber updatedDelete_subscriber = delete_subscriberRepository.findById(delete_subscriber.getId()).get();
        // Disconnect from session so that the updates on updatedDelete_subscriber are not directly saved in db
        em.detach(updatedDelete_subscriber);
        updatedDelete_subscriber
            .msisdn(UPDATED_MSISDN)
            .imsi(UPDATED_IMSI)
            .platform(UPDATED_PLATFORM)
            .comments(UPDATED_COMMENTS);

        restDelete_subscriberMockMvc.perform(put("/api/delete-subscribers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDelete_subscriber)))
            .andExpect(status().isOk());

        // Validate the Delete_subscriber in the database
        List<Delete_subscriber> delete_subscriberList = delete_subscriberRepository.findAll();
        assertThat(delete_subscriberList).hasSize(databaseSizeBeforeUpdate);
        Delete_subscriber testDelete_subscriber = delete_subscriberList.get(delete_subscriberList.size() - 1);
        assertThat(testDelete_subscriber.getMsisdn()).isEqualTo(UPDATED_MSISDN);
        assertThat(testDelete_subscriber.getImsi()).isEqualTo(UPDATED_IMSI);
        assertThat(testDelete_subscriber.getPlatform()).isEqualTo(UPDATED_PLATFORM);
        assertThat(testDelete_subscriber.getComments()).isEqualTo(UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void updateNonExistingDelete_subscriber() throws Exception {
        int databaseSizeBeforeUpdate = delete_subscriberRepository.findAll().size();

        // Create the Delete_subscriber

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDelete_subscriberMockMvc.perform(put("/api/delete-subscribers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(delete_subscriber)))
            .andExpect(status().isBadRequest());

        // Validate the Delete_subscriber in the database
        List<Delete_subscriber> delete_subscriberList = delete_subscriberRepository.findAll();
        assertThat(delete_subscriberList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDelete_subscriber() throws Exception {
        // Initialize the database
        delete_subscriberService.save(delete_subscriber);

        int databaseSizeBeforeDelete = delete_subscriberRepository.findAll().size();

        // Delete the delete_subscriber
        restDelete_subscriberMockMvc.perform(delete("/api/delete-subscribers/{id}", delete_subscriber.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Delete_subscriber> delete_subscriberList = delete_subscriberRepository.findAll();
        assertThat(delete_subscriberList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Delete_subscriber.class);
        Delete_subscriber delete_subscriber1 = new Delete_subscriber();
        delete_subscriber1.setId(1L);
        Delete_subscriber delete_subscriber2 = new Delete_subscriber();
        delete_subscriber2.setId(delete_subscriber1.getId());
        assertThat(delete_subscriber1).isEqualTo(delete_subscriber2);
        delete_subscriber2.setId(2L);
        assertThat(delete_subscriber1).isNotEqualTo(delete_subscriber2);
        delete_subscriber1.setId(null);
        assertThat(delete_subscriber1).isNotEqualTo(delete_subscriber2);
    }
}
