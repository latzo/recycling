package free.sn.web.rest;

import free.sn.RecyclingApp;
import free.sn.domain.Exclude_number;
import free.sn.repository.Exclude_numberRepository;
import free.sn.service.Exclude_numberService;
import free.sn.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static free.sn.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link Exclude_numberResource} REST controller.
 */
@SpringBootTest(classes = RecyclingApp.class)
public class Exclude_numberResourceIT {

    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";

    @Autowired
    private Exclude_numberRepository exclude_numberRepository;

    @Autowired
    private Exclude_numberService exclude_numberService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restExclude_numberMockMvc;

    private Exclude_number exclude_number;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final Exclude_numberResource exclude_numberResource = new Exclude_numberResource(exclude_numberService);
        this.restExclude_numberMockMvc = MockMvcBuilders.standaloneSetup(exclude_numberResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Exclude_number createEntity(EntityManager em) {
        Exclude_number exclude_number = new Exclude_number()
            .msisdn(DEFAULT_MSISDN)
            .comments(DEFAULT_COMMENTS);
        return exclude_number;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Exclude_number createUpdatedEntity(EntityManager em) {
        Exclude_number exclude_number = new Exclude_number()
            .msisdn(UPDATED_MSISDN)
            .comments(UPDATED_COMMENTS);
        return exclude_number;
    }

    @BeforeEach
    public void initTest() {
        exclude_number = createEntity(em);
    }

    @Test
    @Transactional
    public void createExclude_number() throws Exception {
        int databaseSizeBeforeCreate = exclude_numberRepository.findAll().size();

        // Create the Exclude_number
        restExclude_numberMockMvc.perform(post("/api/exclude-numbers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(exclude_number)))
            .andExpect(status().isCreated());

        // Validate the Exclude_number in the database
        List<Exclude_number> exclude_numberList = exclude_numberRepository.findAll();
        assertThat(exclude_numberList).hasSize(databaseSizeBeforeCreate + 1);
        Exclude_number testExclude_number = exclude_numberList.get(exclude_numberList.size() - 1);
        assertThat(testExclude_number.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
        assertThat(testExclude_number.getComments()).isEqualTo(DEFAULT_COMMENTS);
    }

    @Test
    @Transactional
    public void createExclude_numberWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = exclude_numberRepository.findAll().size();

        // Create the Exclude_number with an existing ID
        exclude_number.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExclude_numberMockMvc.perform(post("/api/exclude-numbers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(exclude_number)))
            .andExpect(status().isBadRequest());

        // Validate the Exclude_number in the database
        List<Exclude_number> exclude_numberList = exclude_numberRepository.findAll();
        assertThat(exclude_numberList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllExclude_numbers() throws Exception {
        // Initialize the database
        exclude_numberRepository.saveAndFlush(exclude_number);

        // Get all the exclude_numberList
        restExclude_numberMockMvc.perform(get("/api/exclude-numbers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(exclude_number.getId().intValue())))
            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS.toString())));
    }
    
    @Test
    @Transactional
    public void getExclude_number() throws Exception {
        // Initialize the database
        exclude_numberRepository.saveAndFlush(exclude_number);

        // Get the exclude_number
        restExclude_numberMockMvc.perform(get("/api/exclude-numbers/{id}", exclude_number.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(exclude_number.getId().intValue()))
            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingExclude_number() throws Exception {
        // Get the exclude_number
        restExclude_numberMockMvc.perform(get("/api/exclude-numbers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExclude_number() throws Exception {
        // Initialize the database
        exclude_numberService.save(exclude_number);

        int databaseSizeBeforeUpdate = exclude_numberRepository.findAll().size();

        // Update the exclude_number
        Exclude_number updatedExclude_number = exclude_numberRepository.findById(exclude_number.getId()).get();
        // Disconnect from session so that the updates on updatedExclude_number are not directly saved in db
        em.detach(updatedExclude_number);
        updatedExclude_number
            .msisdn(UPDATED_MSISDN)
            .comments(UPDATED_COMMENTS);

        restExclude_numberMockMvc.perform(put("/api/exclude-numbers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedExclude_number)))
            .andExpect(status().isOk());

        // Validate the Exclude_number in the database
        List<Exclude_number> exclude_numberList = exclude_numberRepository.findAll();
        assertThat(exclude_numberList).hasSize(databaseSizeBeforeUpdate);
        Exclude_number testExclude_number = exclude_numberList.get(exclude_numberList.size() - 1);
        assertThat(testExclude_number.getMsisdn()).isEqualTo(UPDATED_MSISDN);
        assertThat(testExclude_number.getComments()).isEqualTo(UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void updateNonExistingExclude_number() throws Exception {
        int databaseSizeBeforeUpdate = exclude_numberRepository.findAll().size();

        // Create the Exclude_number

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExclude_numberMockMvc.perform(put("/api/exclude-numbers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(exclude_number)))
            .andExpect(status().isBadRequest());

        // Validate the Exclude_number in the database
        List<Exclude_number> exclude_numberList = exclude_numberRepository.findAll();
        assertThat(exclude_numberList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteExclude_number() throws Exception {
        // Initialize the database
        exclude_numberService.save(exclude_number);

        int databaseSizeBeforeDelete = exclude_numberRepository.findAll().size();

        // Delete the exclude_number
        restExclude_numberMockMvc.perform(delete("/api/exclude-numbers/{id}", exclude_number.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Exclude_number> exclude_numberList = exclude_numberRepository.findAll();
        assertThat(exclude_numberList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Exclude_number.class);
        Exclude_number exclude_number1 = new Exclude_number();
        exclude_number1.setId(1L);
        Exclude_number exclude_number2 = new Exclude_number();
        exclude_number2.setId(exclude_number1.getId());
        assertThat(exclude_number1).isEqualTo(exclude_number2);
        exclude_number2.setId(2L);
        assertThat(exclude_number1).isNotEqualTo(exclude_number2);
        exclude_number1.setId(null);
        assertThat(exclude_number1).isNotEqualTo(exclude_number2);
    }
}
