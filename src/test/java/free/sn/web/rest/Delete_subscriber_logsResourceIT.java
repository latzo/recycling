package free.sn.web.rest;

import free.sn.RecyclingApp;
import free.sn.domain.Delete_subscriber_logs;
import free.sn.repository.Delete_subscriber_logsRepository;
import free.sn.service.Delete_subscriber_logsService;
import free.sn.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static free.sn.web.rest.TestUtil.sameInstant;
import static free.sn.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link Delete_subscriber_logsResource} REST controller.
 */
@SpringBootTest(classes = RecyclingApp.class)
public class Delete_subscriber_logsResourceIT {

    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_IMSI = "AAAAAAAAAA";
    private static final String UPDATED_IMSI = "BBBBBBBBBB";

    private static final String DEFAULT_PLATFORM = "AAAAAAAAAA";
    private static final String UPDATED_PLATFORM = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DELETED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DELETED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_DELETED_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_DELETED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_REQUEST_SENT = "AAAAAAAAAA";
    private static final String UPDATED_REQUEST_SENT = "BBBBBBBBBB";

    private static final String DEFAULT_PLATFORM_RESPONSE = "AAAAAAAAAA";
    private static final String UPDATED_PLATFORM_RESPONSE = "BBBBBBBBBB";

    private static final String DEFAULT_SESSION_ID = "AAAAAAAAAA";
    private static final String UPDATED_SESSION_ID = "BBBBBBBBBB";

    @Autowired
    private Delete_subscriber_logsRepository delete_subscriber_logsRepository;

    @Autowired
    private Delete_subscriber_logsService delete_subscriber_logsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDelete_subscriber_logsMockMvc;

    private Delete_subscriber_logs delete_subscriber_logs;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final Delete_subscriber_logsResource delete_subscriber_logsResource = new Delete_subscriber_logsResource(delete_subscriber_logsService);
        this.restDelete_subscriber_logsMockMvc = MockMvcBuilders.standaloneSetup(delete_subscriber_logsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Delete_subscriber_logs createEntity(EntityManager em) {
        Delete_subscriber_logs delete_subscriber_logs = new Delete_subscriber_logs()
            .msisdn(DEFAULT_MSISDN)
            .imsi(DEFAULT_IMSI)
            .platform(DEFAULT_PLATFORM)
            .deleted_date(DEFAULT_DELETED_DATE)
            .deleted_status(DEFAULT_DELETED_STATUS)
            .request_sent(DEFAULT_REQUEST_SENT)
            .platform_response(DEFAULT_PLATFORM_RESPONSE)
            .session_id(DEFAULT_SESSION_ID);
        return delete_subscriber_logs;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Delete_subscriber_logs createUpdatedEntity(EntityManager em) {
        Delete_subscriber_logs delete_subscriber_logs = new Delete_subscriber_logs()
            .msisdn(UPDATED_MSISDN)
            .imsi(UPDATED_IMSI)
            .platform(UPDATED_PLATFORM)
            .deleted_date(UPDATED_DELETED_DATE)
            .deleted_status(UPDATED_DELETED_STATUS)
            .request_sent(UPDATED_REQUEST_SENT)
            .platform_response(UPDATED_PLATFORM_RESPONSE)
            .session_id(UPDATED_SESSION_ID);
        return delete_subscriber_logs;
    }

    @BeforeEach
    public void initTest() {
        delete_subscriber_logs = createEntity(em);
    }

    @Test
    @Transactional
    public void createDelete_subscriber_logs() throws Exception {
        int databaseSizeBeforeCreate = delete_subscriber_logsRepository.findAll().size();

        // Create the Delete_subscriber_logs
        restDelete_subscriber_logsMockMvc.perform(post("/api/delete-subscriber-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(delete_subscriber_logs)))
            .andExpect(status().isCreated());

        // Validate the Delete_subscriber_logs in the database
        List<Delete_subscriber_logs> delete_subscriber_logsList = delete_subscriber_logsRepository.findAll();
        assertThat(delete_subscriber_logsList).hasSize(databaseSizeBeforeCreate + 1);
        Delete_subscriber_logs testDelete_subscriber_logs = delete_subscriber_logsList.get(delete_subscriber_logsList.size() - 1);
        assertThat(testDelete_subscriber_logs.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
        assertThat(testDelete_subscriber_logs.getImsi()).isEqualTo(DEFAULT_IMSI);
        assertThat(testDelete_subscriber_logs.getPlatform()).isEqualTo(DEFAULT_PLATFORM);
        assertThat(testDelete_subscriber_logs.getDeleted_date()).isEqualTo(DEFAULT_DELETED_DATE);
        assertThat(testDelete_subscriber_logs.getDeleted_status()).isEqualTo(DEFAULT_DELETED_STATUS);
        assertThat(testDelete_subscriber_logs.getRequest_sent()).isEqualTo(DEFAULT_REQUEST_SENT);
        assertThat(testDelete_subscriber_logs.getPlatform_response()).isEqualTo(DEFAULT_PLATFORM_RESPONSE);
        assertThat(testDelete_subscriber_logs.getSession_id()).isEqualTo(DEFAULT_SESSION_ID);
    }

    @Test
    @Transactional
    public void createDelete_subscriber_logsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = delete_subscriber_logsRepository.findAll().size();

        // Create the Delete_subscriber_logs with an existing ID
        delete_subscriber_logs.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDelete_subscriber_logsMockMvc.perform(post("/api/delete-subscriber-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(delete_subscriber_logs)))
            .andExpect(status().isBadRequest());

        // Validate the Delete_subscriber_logs in the database
        List<Delete_subscriber_logs> delete_subscriber_logsList = delete_subscriber_logsRepository.findAll();
        assertThat(delete_subscriber_logsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDelete_subscriber_logs() throws Exception {
        // Initialize the database
        delete_subscriber_logsRepository.saveAndFlush(delete_subscriber_logs);

        // Get all the delete_subscriber_logsList
        restDelete_subscriber_logsMockMvc.perform(get("/api/delete-subscriber-logs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(delete_subscriber_logs.getId().intValue())))
            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
            .andExpect(jsonPath("$.[*].imsi").value(hasItem(DEFAULT_IMSI.toString())))
            .andExpect(jsonPath("$.[*].platform").value(hasItem(DEFAULT_PLATFORM.toString())))
            .andExpect(jsonPath("$.[*].deleted_date").value(hasItem(sameInstant(DEFAULT_DELETED_DATE))))
            .andExpect(jsonPath("$.[*].deleted_status").value(hasItem(DEFAULT_DELETED_STATUS.toString())))
            .andExpect(jsonPath("$.[*].request_sent").value(hasItem(DEFAULT_REQUEST_SENT.toString())))
            .andExpect(jsonPath("$.[*].platform_response").value(hasItem(DEFAULT_PLATFORM_RESPONSE.toString())))
            .andExpect(jsonPath("$.[*].session_id").value(hasItem(DEFAULT_SESSION_ID.toString())));
    }
    
    @Test
    @Transactional
    public void getDelete_subscriber_logs() throws Exception {
        // Initialize the database
        delete_subscriber_logsRepository.saveAndFlush(delete_subscriber_logs);

        // Get the delete_subscriber_logs
        restDelete_subscriber_logsMockMvc.perform(get("/api/delete-subscriber-logs/{id}", delete_subscriber_logs.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(delete_subscriber_logs.getId().intValue()))
            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
            .andExpect(jsonPath("$.imsi").value(DEFAULT_IMSI.toString()))
            .andExpect(jsonPath("$.platform").value(DEFAULT_PLATFORM.toString()))
            .andExpect(jsonPath("$.deleted_date").value(sameInstant(DEFAULT_DELETED_DATE)))
            .andExpect(jsonPath("$.deleted_status").value(DEFAULT_DELETED_STATUS.toString()))
            .andExpect(jsonPath("$.request_sent").value(DEFAULT_REQUEST_SENT.toString()))
            .andExpect(jsonPath("$.platform_response").value(DEFAULT_PLATFORM_RESPONSE.toString()))
            .andExpect(jsonPath("$.session_id").value(DEFAULT_SESSION_ID.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDelete_subscriber_logs() throws Exception {
        // Get the delete_subscriber_logs
        restDelete_subscriber_logsMockMvc.perform(get("/api/delete-subscriber-logs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDelete_subscriber_logs() throws Exception {
        // Initialize the database
        delete_subscriber_logsService.save(delete_subscriber_logs);

        int databaseSizeBeforeUpdate = delete_subscriber_logsRepository.findAll().size();

        // Update the delete_subscriber_logs
        Delete_subscriber_logs updatedDelete_subscriber_logs = delete_subscriber_logsRepository.findById(delete_subscriber_logs.getId()).get();
        // Disconnect from session so that the updates on updatedDelete_subscriber_logs are not directly saved in db
        em.detach(updatedDelete_subscriber_logs);
        updatedDelete_subscriber_logs
            .msisdn(UPDATED_MSISDN)
            .imsi(UPDATED_IMSI)
            .platform(UPDATED_PLATFORM)
            .deleted_date(UPDATED_DELETED_DATE)
            .deleted_status(UPDATED_DELETED_STATUS)
            .request_sent(UPDATED_REQUEST_SENT)
            .platform_response(UPDATED_PLATFORM_RESPONSE)
            .session_id(UPDATED_SESSION_ID);

        restDelete_subscriber_logsMockMvc.perform(put("/api/delete-subscriber-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDelete_subscriber_logs)))
            .andExpect(status().isOk());

        // Validate the Delete_subscriber_logs in the database
        List<Delete_subscriber_logs> delete_subscriber_logsList = delete_subscriber_logsRepository.findAll();
        assertThat(delete_subscriber_logsList).hasSize(databaseSizeBeforeUpdate);
        Delete_subscriber_logs testDelete_subscriber_logs = delete_subscriber_logsList.get(delete_subscriber_logsList.size() - 1);
        assertThat(testDelete_subscriber_logs.getMsisdn()).isEqualTo(UPDATED_MSISDN);
        assertThat(testDelete_subscriber_logs.getImsi()).isEqualTo(UPDATED_IMSI);
        assertThat(testDelete_subscriber_logs.getPlatform()).isEqualTo(UPDATED_PLATFORM);
        assertThat(testDelete_subscriber_logs.getDeleted_date()).isEqualTo(UPDATED_DELETED_DATE);
        assertThat(testDelete_subscriber_logs.getDeleted_status()).isEqualTo(UPDATED_DELETED_STATUS);
        assertThat(testDelete_subscriber_logs.getRequest_sent()).isEqualTo(UPDATED_REQUEST_SENT);
        assertThat(testDelete_subscriber_logs.getPlatform_response()).isEqualTo(UPDATED_PLATFORM_RESPONSE);
        assertThat(testDelete_subscriber_logs.getSession_id()).isEqualTo(UPDATED_SESSION_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingDelete_subscriber_logs() throws Exception {
        int databaseSizeBeforeUpdate = delete_subscriber_logsRepository.findAll().size();

        // Create the Delete_subscriber_logs

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDelete_subscriber_logsMockMvc.perform(put("/api/delete-subscriber-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(delete_subscriber_logs)))
            .andExpect(status().isBadRequest());

        // Validate the Delete_subscriber_logs in the database
        List<Delete_subscriber_logs> delete_subscriber_logsList = delete_subscriber_logsRepository.findAll();
        assertThat(delete_subscriber_logsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDelete_subscriber_logs() throws Exception {
        // Initialize the database
        delete_subscriber_logsService.save(delete_subscriber_logs);

        int databaseSizeBeforeDelete = delete_subscriber_logsRepository.findAll().size();

        // Delete the delete_subscriber_logs
        restDelete_subscriber_logsMockMvc.perform(delete("/api/delete-subscriber-logs/{id}", delete_subscriber_logs.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Delete_subscriber_logs> delete_subscriber_logsList = delete_subscriber_logsRepository.findAll();
        assertThat(delete_subscriber_logsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Delete_subscriber_logs.class);
        Delete_subscriber_logs delete_subscriber_logs1 = new Delete_subscriber_logs();
        delete_subscriber_logs1.setId(1L);
        Delete_subscriber_logs delete_subscriber_logs2 = new Delete_subscriber_logs();
        delete_subscriber_logs2.setId(delete_subscriber_logs1.getId());
        assertThat(delete_subscriber_logs1).isEqualTo(delete_subscriber_logs2);
        delete_subscriber_logs2.setId(2L);
        assertThat(delete_subscriber_logs1).isNotEqualTo(delete_subscriber_logs2);
        delete_subscriber_logs1.setId(null);
        assertThat(delete_subscriber_logs1).isNotEqualTo(delete_subscriber_logs2);
    }
}
