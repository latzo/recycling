/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RecyclingTestModule } from '../../../test.module';
import { Delete_subscriber_logsDetailComponent } from 'app/entities/delete-subscriber-logs/delete-subscriber-logs-detail.component';
import { Delete_subscriber_logs } from 'app/shared/model/delete-subscriber-logs.model';

describe('Component Tests', () => {
  describe('Delete_subscriber_logs Management Detail Component', () => {
    let comp: Delete_subscriber_logsDetailComponent;
    let fixture: ComponentFixture<Delete_subscriber_logsDetailComponent>;
    const route = ({ data: of({ delete_subscriber_logs: new Delete_subscriber_logs(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecyclingTestModule],
        declarations: [Delete_subscriber_logsDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(Delete_subscriber_logsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(Delete_subscriber_logsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.delete_subscriber_logs).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
