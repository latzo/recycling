/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { RecyclingTestModule } from '../../../test.module';
import { Delete_subscriber_logsDeleteDialogComponent } from 'app/entities/delete-subscriber-logs/delete-subscriber-logs-delete-dialog.component';
import { Delete_subscriber_logsService } from 'app/entities/delete-subscriber-logs/delete-subscriber-logs.service';

describe('Component Tests', () => {
  describe('Delete_subscriber_logs Management Delete Component', () => {
    let comp: Delete_subscriber_logsDeleteDialogComponent;
    let fixture: ComponentFixture<Delete_subscriber_logsDeleteDialogComponent>;
    let service: Delete_subscriber_logsService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecyclingTestModule],
        declarations: [Delete_subscriber_logsDeleteDialogComponent]
      })
        .overrideTemplate(Delete_subscriber_logsDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(Delete_subscriber_logsDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(Delete_subscriber_logsService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
