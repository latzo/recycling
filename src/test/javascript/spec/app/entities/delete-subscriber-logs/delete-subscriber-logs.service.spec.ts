/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { Delete_subscriber_logsService } from 'app/entities/delete-subscriber-logs/delete-subscriber-logs.service';
import { IDelete_subscriber_logs, Delete_subscriber_logs } from 'app/shared/model/delete-subscriber-logs.model';

describe('Service Tests', () => {
  describe('Delete_subscriber_logs Service', () => {
    let injector: TestBed;
    let service: Delete_subscriber_logsService;
    let httpMock: HttpTestingController;
    let elemDefault: IDelete_subscriber_logs;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(Delete_subscriber_logsService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Delete_subscriber_logs(0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', currentDate, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            deleted_date: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Delete_subscriber_logs', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            deleted_date: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            deleted_date: currentDate
          },
          returnedFromService
        );
        service
          .create(new Delete_subscriber_logs(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Delete_subscriber_logs', async () => {
        const returnedFromService = Object.assign(
          {
            msisdn: 'BBBBBB',
            imsi: 'BBBBBB',
            platform: 'BBBBBB',
            deleted_date: currentDate.format(DATE_TIME_FORMAT),
            deleted_status: 'BBBBBB',
            request_sent: 'BBBBBB',
            platform_response: 'BBBBBB',
            session_id: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            deleted_date: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Delete_subscriber_logs', async () => {
        const returnedFromService = Object.assign(
          {
            msisdn: 'BBBBBB',
            imsi: 'BBBBBB',
            platform: 'BBBBBB',
            deleted_date: currentDate.format(DATE_TIME_FORMAT),
            deleted_status: 'BBBBBB',
            request_sent: 'BBBBBB',
            platform_response: 'BBBBBB',
            session_id: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            deleted_date: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Delete_subscriber_logs', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
