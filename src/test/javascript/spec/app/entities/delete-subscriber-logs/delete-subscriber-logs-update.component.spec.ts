/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { RecyclingTestModule } from '../../../test.module';
import { Delete_subscriber_logsUpdateComponent } from 'app/entities/delete-subscriber-logs/delete-subscriber-logs-update.component';
import { Delete_subscriber_logsService } from 'app/entities/delete-subscriber-logs/delete-subscriber-logs.service';
import { Delete_subscriber_logs } from 'app/shared/model/delete-subscriber-logs.model';

describe('Component Tests', () => {
  describe('Delete_subscriber_logs Management Update Component', () => {
    let comp: Delete_subscriber_logsUpdateComponent;
    let fixture: ComponentFixture<Delete_subscriber_logsUpdateComponent>;
    let service: Delete_subscriber_logsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecyclingTestModule],
        declarations: [Delete_subscriber_logsUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(Delete_subscriber_logsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(Delete_subscriber_logsUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(Delete_subscriber_logsService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Delete_subscriber_logs(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Delete_subscriber_logs();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
