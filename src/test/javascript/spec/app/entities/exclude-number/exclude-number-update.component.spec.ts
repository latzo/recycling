/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { RecyclingTestModule } from '../../../test.module';
import { Exclude_numberUpdateComponent } from 'app/entities/exclude-number/exclude-number-update.component';
import { Exclude_numberService } from 'app/entities/exclude-number/exclude-number.service';
import { Exclude_number } from 'app/shared/model/exclude-number.model';

describe('Component Tests', () => {
  describe('Exclude_number Management Update Component', () => {
    let comp: Exclude_numberUpdateComponent;
    let fixture: ComponentFixture<Exclude_numberUpdateComponent>;
    let service: Exclude_numberService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecyclingTestModule],
        declarations: [Exclude_numberUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(Exclude_numberUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(Exclude_numberUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(Exclude_numberService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Exclude_number(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Exclude_number();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
