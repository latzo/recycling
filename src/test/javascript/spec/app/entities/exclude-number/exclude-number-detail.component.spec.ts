/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RecyclingTestModule } from '../../../test.module';
import { Exclude_numberDetailComponent } from 'app/entities/exclude-number/exclude-number-detail.component';
import { Exclude_number } from 'app/shared/model/exclude-number.model';

describe('Component Tests', () => {
  describe('Exclude_number Management Detail Component', () => {
    let comp: Exclude_numberDetailComponent;
    let fixture: ComponentFixture<Exclude_numberDetailComponent>;
    const route = ({ data: of({ exclude_number: new Exclude_number(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecyclingTestModule],
        declarations: [Exclude_numberDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(Exclude_numberDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(Exclude_numberDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.exclude_number).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
