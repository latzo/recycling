/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { RecyclingTestModule } from '../../../test.module';
import { Exclude_numberDeleteDialogComponent } from 'app/entities/exclude-number/exclude-number-delete-dialog.component';
import { Exclude_numberService } from 'app/entities/exclude-number/exclude-number.service';

describe('Component Tests', () => {
  describe('Exclude_number Management Delete Component', () => {
    let comp: Exclude_numberDeleteDialogComponent;
    let fixture: ComponentFixture<Exclude_numberDeleteDialogComponent>;
    let service: Exclude_numberService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecyclingTestModule],
        declarations: [Exclude_numberDeleteDialogComponent]
      })
        .overrideTemplate(Exclude_numberDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(Exclude_numberDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(Exclude_numberService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
