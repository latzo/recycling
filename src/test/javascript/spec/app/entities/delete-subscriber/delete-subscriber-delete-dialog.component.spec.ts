/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { RecyclingTestModule } from '../../../test.module';
import { Delete_subscriberDeleteDialogComponent } from 'app/entities/delete-subscriber/delete-subscriber-delete-dialog.component';
import { Delete_subscriberService } from 'app/entities/delete-subscriber/delete-subscriber.service';

describe('Component Tests', () => {
  describe('Delete_subscriber Management Delete Component', () => {
    let comp: Delete_subscriberDeleteDialogComponent;
    let fixture: ComponentFixture<Delete_subscriberDeleteDialogComponent>;
    let service: Delete_subscriberService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecyclingTestModule],
        declarations: [Delete_subscriberDeleteDialogComponent]
      })
        .overrideTemplate(Delete_subscriberDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(Delete_subscriberDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(Delete_subscriberService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
