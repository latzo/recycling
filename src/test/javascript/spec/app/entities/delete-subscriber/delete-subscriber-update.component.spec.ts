/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { RecyclingTestModule } from '../../../test.module';
import { Delete_subscriberUpdateComponent } from 'app/entities/delete-subscriber/delete-subscriber-update.component';
import { Delete_subscriberService } from 'app/entities/delete-subscriber/delete-subscriber.service';
import { Delete_subscriber } from 'app/shared/model/delete-subscriber.model';

describe('Component Tests', () => {
  describe('Delete_subscriber Management Update Component', () => {
    let comp: Delete_subscriberUpdateComponent;
    let fixture: ComponentFixture<Delete_subscriberUpdateComponent>;
    let service: Delete_subscriberService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecyclingTestModule],
        declarations: [Delete_subscriberUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(Delete_subscriberUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(Delete_subscriberUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(Delete_subscriberService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Delete_subscriber(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Delete_subscriber();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
