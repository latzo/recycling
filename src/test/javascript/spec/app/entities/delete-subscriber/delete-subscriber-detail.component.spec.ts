/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RecyclingTestModule } from '../../../test.module';
import { Delete_subscriberDetailComponent } from 'app/entities/delete-subscriber/delete-subscriber-detail.component';
import { Delete_subscriber } from 'app/shared/model/delete-subscriber.model';

describe('Component Tests', () => {
  describe('Delete_subscriber Management Detail Component', () => {
    let comp: Delete_subscriberDetailComponent;
    let fixture: ComponentFixture<Delete_subscriberDetailComponent>;
    const route = ({ data: of({ delete_subscriber: new Delete_subscriber(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecyclingTestModule],
        declarations: [Delete_subscriberDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(Delete_subscriberDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(Delete_subscriberDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.delete_subscriber).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
