/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RecyclingTestModule } from '../../../test.module';
import { Deleted_subscriberDetailComponent } from 'app/entities/deleted-subscriber/deleted-subscriber-detail.component';
import { Deleted_subscriber } from 'app/shared/model/deleted-subscriber.model';

describe('Component Tests', () => {
  describe('Deleted_subscriber Management Detail Component', () => {
    let comp: Deleted_subscriberDetailComponent;
    let fixture: ComponentFixture<Deleted_subscriberDetailComponent>;
    const route = ({ data: of({ deleted_subscriber: new Deleted_subscriber(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecyclingTestModule],
        declarations: [Deleted_subscriberDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(Deleted_subscriberDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(Deleted_subscriberDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.deleted_subscriber).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
