/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { RecyclingTestModule } from '../../../test.module';
import { Deleted_subscriberDeleteDialogComponent } from 'app/entities/deleted-subscriber/deleted-subscriber-delete-dialog.component';
import { Deleted_subscriberService } from 'app/entities/deleted-subscriber/deleted-subscriber.service';

describe('Component Tests', () => {
  describe('Deleted_subscriber Management Delete Component', () => {
    let comp: Deleted_subscriberDeleteDialogComponent;
    let fixture: ComponentFixture<Deleted_subscriberDeleteDialogComponent>;
    let service: Deleted_subscriberService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecyclingTestModule],
        declarations: [Deleted_subscriberDeleteDialogComponent]
      })
        .overrideTemplate(Deleted_subscriberDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(Deleted_subscriberDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(Deleted_subscriberService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
