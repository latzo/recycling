/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { RecyclingTestModule } from '../../../test.module';
import { Deleted_subscriberUpdateComponent } from 'app/entities/deleted-subscriber/deleted-subscriber-update.component';
import { Deleted_subscriberService } from 'app/entities/deleted-subscriber/deleted-subscriber.service';
import { Deleted_subscriber } from 'app/shared/model/deleted-subscriber.model';

describe('Component Tests', () => {
  describe('Deleted_subscriber Management Update Component', () => {
    let comp: Deleted_subscriberUpdateComponent;
    let fixture: ComponentFixture<Deleted_subscriberUpdateComponent>;
    let service: Deleted_subscriberService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecyclingTestModule],
        declarations: [Deleted_subscriberUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(Deleted_subscriberUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(Deleted_subscriberUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(Deleted_subscriberService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Deleted_subscriber(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Deleted_subscriber();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
